#ifndef SVEXEC_MODULE_H
#define SVEXEC_MODULE_H

#include <svexec/processor.h>
#include <stdlib.h>
#include <stdbool.h>


struct Lisbeth_SynthModuleConnection_;
struct Lisbeth_SynthModule_;
struct Lisbeth_SynthModuleBuiltin_;
struct Lisbeth_SynthModuleGroup_;
struct Lisbeth_SynthModuleProgram_;


enum Lisbeth_SynthModuleType_
{
    LISBETH_SYNTHMODULE_NONE,
    LISBETH_SYNTHMODULE_BUILTIN,
    LISBETH_SYNTHMODULE_BINARY,
    LISBETH_SYNTHMODULE_GROUP,
};
typedef enum Lisbeth_SynthModuleType_ Lisbeth_SynthModuleType;


typedef void (*Lisbeth_BuiltinModuleFunction)(struct Lisbeth_SynthModule_*);


struct Lisbeth_SynthModuleBuiltin_
{
    Lisbeth_BuiltinModuleFunction create;
    Lisbeth_BuiltinModuleFunction destroy;
    Lisbeth_BuiltinModuleFunction tick;
    void *data;
};
typedef struct Lisbeth_SynthModuleBuiltin_ Lisbeth_SynthModuleBuiltin;


struct Lisbeth_SynthModule_
{
    char *name;
    Lisbeth_SynthModuleType type;
    union
    {
        Lisbeth_SynthModuleBuiltin *builtin;
        struct Lisbeth_SynthModuleProgram_ *program;
        struct Lisbeth_SynthModuleGroup_ *group;
    } content;
    struct Lisbeth_SynthModuleConnection_ **inputs;
    size_t inputCount;
    struct Lisbeth_SynthModuleConnection_ **outputs;
    size_t outputCount;
};
typedef struct Lisbeth_SynthModule_ Lisbeth_SynthModule;


Lisbeth_SynthModule* Lisbeth_synthModule_create(char const *name, Lisbeth_SynthModuleType type, void *data);

void Lisbeth_synthModule_destroy(Lisbeth_SynthModule *module);

struct Lisbeth_SynthModuleConnection_* Lisbeth_synthModule_getInputConnection(Lisbeth_SynthModule *module, char *name);

struct Lisbeth_SynthModuleConnection_* Lisbeth_synthModule_getOutputConnection(Lisbeth_SynthModule *module, char *name);

void Lisbeth_synthModule_addInputConnection(Lisbeth_SynthModule *module, struct Lisbeth_SynthModuleConnection_ *connection);

void Lisbeth_synthModule_addOutputConnection(Lisbeth_SynthModule *module, struct Lisbeth_SynthModuleConnection_ *connection);

void Lisbeth_synthModule_tick(Lisbeth_SynthModule *module);


#endif //SVEXEC_MODULE_H
