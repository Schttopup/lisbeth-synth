#include "stack.h"
#include <sdefs.h>
#include <string.h>


Lisbeth_SynthFileStackItemRoot* Lisbeth_synthFileStackItemRoot_create(void)
{
    Lisbeth_SynthFileStackItemRoot *item = NULL;
    MMALLOC(item, Lisbeth_SynthFileStackItemRoot, 1);
    item->index = 0;
    return item;
}

void Lisbeth_synthFileStackItemRoot_destroy(Lisbeth_SynthFileStackItemRoot *item)
{
    if(!item)
        return;
    FFREE(item);
}

Lisbeth_SynthFileStackItemMacro* Lisbeth_synthFileStackItemMacro_create(void)
{
    return NULL;
}

void Lisbeth_synthFileStackItemMacro_destroy(Lisbeth_SynthFileStackItemMacro *item)
{

}

Lisbeth_SynthFileStackItemLoop* Lisbeth_synthFileStackItemLoop_create(size_t start, size_t end, unsigned int steps, char *name)
{
    Lisbeth_SynthFileStackItemLoop *item = NULL;
    MMALLOC(item, Lisbeth_SynthFileStackItemLoop, 1);
    item->start = start;
    item->end = end;
    item->steps = steps;
    item->curStep = 0;
    item->name = NULL;
    if(name)
        item->name = strdup(name);
    return item;
}

void Lisbeth_synthFileStackItemLoop_destroy(Lisbeth_SynthFileStackItemLoop *item)
{
    if(!item)
        return;
    FFREE(item->name);
    FFREE(item);
}

Lisbeth_SynthFileStackItemCond* Lisbeth_synthFileStackItemCond_create(size_t index)
{
    Lisbeth_SynthFileStackItemCond *item = NULL;
    MMALLOC(item, Lisbeth_SynthFileStackItemCond, 1);
    item->index = index;
    return item;
}

void Lisbeth_synthFileStackItemCond_destroy(Lisbeth_SynthFileStackItemCond *item)
{
    if(!item)
        return;
    FFREE(item);
}


Lisbeth_SynthFileStackItem* Lisbeth_synthFileStackItem_create(Lisbeth_SynthFileStackItemType type, void *content)
{
    if(!type || !content)
        return NULL;
    Lisbeth_SynthFileStackItem *item = NULL;
    MMALLOC(item, Lisbeth_SynthFileStackItem, 1);
    item->type = type;
    switch(type)
    {
        case LISBETH_STACK_ROOT:
            item->value.root = (Lisbeth_SynthFileStackItemRoot*)content;
            break;
        case LISBETH_STACK_MACRO:
            item->value.macro = (Lisbeth_SynthFileStackItemMacro*)content;
            break;
        case LISBETH_STACK_LOOP:
            item->value.loop = (Lisbeth_SynthFileStackItemLoop*)content;
            break;
        case LISBETH_STACK_COND:
            item->value.cond = (Lisbeth_SynthFileStackItemCond*)content;
            break;
        default:
            FFREE(item);
            return NULL;
            break;
    }
    return item;
}

void Lisbeth_synthFileStackItem_destroy(Lisbeth_SynthFileStackItem *item)
{
    if(!item)
        return;
    switch(item->type)
    {
        case LISBETH_STACK_ROOT:
            Lisbeth_synthFileStackItemRoot_destroy(item->value.root);
            break;
        case LISBETH_STACK_MACRO:
            Lisbeth_synthFileStackItemMacro_destroy(item->value.macro);
            break;
        case LISBETH_STACK_LOOP:
            Lisbeth_synthFileStackItemLoop_destroy(item->value.loop);
            break;
        case LISBETH_STACK_COND:
            Lisbeth_synthFileStackItemCond_destroy(item->value.cond);
            break;
        default:
            break;
    }
    FFREE(item);
}


size_t Lisbeth_synthFileStackItem_getIndex(Lisbeth_SynthFileStackItem *item)
{
    if(!item)
        return 0;
    switch (item->type)
    {
        case LISBETH_STACK_ROOT:
            return ((Lisbeth_SynthFileStackItemRoot*)(item->value.root))->index;
            break;
        case LISBETH_STACK_MACRO:
            return ((Lisbeth_SynthFileStackItemMacro*)(item->value.macro))->index;
            break;
        case LISBETH_STACK_LOOP:
            return ((Lisbeth_SynthFileStackItemLoop*)(item->value.loop))->index;
            break;
        case LISBETH_STACK_COND:
            return ((Lisbeth_SynthFileStackItemCond*)(item->value.cond))->index;
            break;
        default:
            return 0;
            break;
    }
}

void Lisbeth_synthFileStackItem_setIndex(Lisbeth_SynthFileStackItem *item, size_t index)
{
    if(!item)
        return;
    switch (item->type)
    {
        case LISBETH_STACK_ROOT:
            ((Lisbeth_SynthFileStackItemRoot*)(item->value.root))->index = index;
            break;
        case LISBETH_STACK_MACRO:
            ((Lisbeth_SynthFileStackItemMacro*)(item->value.macro))->index = index;
            break;
        case LISBETH_STACK_LOOP:
            ((Lisbeth_SynthFileStackItemLoop*)(item->value.loop))->index = index;
            break;
        case LISBETH_STACK_COND:
            ((Lisbeth_SynthFileStackItemCond*)(item->value.cond))->index = index;
            break;
        default:
            break;
    }
}


Lisbeth_SynthFileStack *Lisbeth_synthFileStack_create(void)
{
    Lisbeth_SynthFileStack *stack = NULL;
    MMALLOC(stack, Lisbeth_SynthFileStack, 1);
    stack->items = NULL;
    stack->count = 0;
    return stack;
}

void Lisbeth_synthFileStack_destroy(Lisbeth_SynthFileStack *stack)
{
    if(!stack)
        return;
    for(size_t i = 0; i < stack->count; i++)
        Lisbeth_synthFileStackItem_destroy(stack->items[i]);
    FFREE(stack->items);
    FFREE(stack);
}

void Lisbeth_synthFileStack_push(Lisbeth_SynthFileStack *stack, Lisbeth_SynthFileStackItem *item)
{
    if(!stack || !item)
        return;
    RREALLOC(stack->items, Lisbeth_SynthFileStackItem*, stack->count + 1);
    stack->items[stack->count] = item;
    stack->count++;
}

void Lisbeth_synthFileStack_pop(Lisbeth_SynthFileStack *stack)
{
    if(!stack)
        return;
    stack->count--;
    Lisbeth_synthFileStackItem_destroy(stack->items[stack->count]);
    RREALLOC(stack->items, Lisbeth_SynthFileStackItem*, stack->count);
}

Lisbeth_SynthFileStackItem* Lisbeth_synthFileStack_top(Lisbeth_SynthFileStack *stack)
{
    if(!stack)
        return NULL;
    if(stack->count == 0)
        return NULL;
    return stack->items[stack->count - 1];
}

