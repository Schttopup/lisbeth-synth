#include "builder.h"
#include <sdefs.h>
#include <string.h>


Lisbeth_SynthFileBuilder* Lisbeth_synthFileBuilder_create(Lisbeth_SynthFileTokenList *tokens, char const *fileName, SLog_Logger *logger)
{
    if(!tokens)
        return NULL;
    Lisbeth_SynthFileBuilder *builder = NULL;
    MMALLOC(builder, Lisbeth_SynthFileBuilder, 1);
    builder->fileName = NULL;
    if(fileName)
    {
        MMALLOC(builder->fileName, char, strlen(fileName) + 1);
        strcpy(builder->fileName, fileName);
    }
    builder->blocks = Lisbeth_synthFileBlockList_create();
    builder->tokens = tokens;
    builder->index = 0;
    builder->logger = logger;
    return builder;
}

void Lisbeth_synthFileBuiler_destroy(Lisbeth_SynthFileBuilder *builder)
{
    if(!builder)
        return;
    Lisbeth_synthFileBlockList_destroy(builder->blocks);
    FFREE(builder->fileName);
    FFREE(builder);
}


Lisbeth_SynthFileBlockList* Lisbeth_synthFileBuilder_build(Lisbeth_SynthFileTokenList *tokens, char const *fileName, SLog_Logger *logger)
{
    if(!tokens)
        return NULL;
    Lisbeth_SynthFileBuilder *builder = Lisbeth_synthFileBuilder_create(tokens, fileName, logger);

    while(builder->index < builder->tokens->tokenCount)
    {
        if(builder->tokens->tokens[builder->index]->type == LISBETH_SYNTHFILETOKEN_NEWLINE)
        {
            Lisbeth_synthFileBuilder_nextLine(builder);
            continue;
        }
        if(!Lisbeth_synthFileBuilder_buildBlock(builder))
        {
            Lisbeth_synthFileBuiler_destroy(builder);
            return NULL;
        }
    }

    Lisbeth_SynthFileBlockList *blocks = builder->blocks;
    builder->blocks = NULL;
    Lisbeth_synthFileBuiler_destroy(builder);
    return blocks;
}


void Lisbeth_synthFileBuilder_nextLine(Lisbeth_SynthFileBuilder *builder)
{
    if(!builder)
        return;
    if(builder->index >= builder->tokens->tokenCount)
        return;
    while (builder->index < builder->tokens->tokenCount && builder->tokens->tokens[builder->index]->type == LISBETH_SYNTHFILETOKEN_NEWLINE)
        builder->index++;
}


Lisbeth_SynthFileModule* Lisbeth_synthFileBuilder_buildModule(Lisbeth_SynthFileBuilder *builder)
{
    if(!builder)
        return NULL;
    if(builder->index >= builder->tokens->tokenCount)
        return NULL;
    size_t line = builder->tokens->tokens[builder->index]->line;
    if(builder->tokens->tokens[builder->index]->type != LISBETH_SYNTHFILETOKEN_MODULE)
    {
        SLog_logger_sendMessage(builder->logger, SLOG_LEVEL_ERROR, "Expected module name. File \"%s\", line %d", builder->fileName, line);
        return NULL;
    }
    char *moduleName = builder->tokens->tokens[builder->index]->content.text;
    // TODO validate module name (only alnum, under & slash)
    Lisbeth_SynthFileModule *module = Lisbeth_synthFileModule_create(moduleName);
    builder->index++;
    if(builder->index < builder->tokens->tokenCount &&
            builder->tokens->tokens[builder->index]->type == LISBETH_SYNTHFILETOKEN_SEPARATOR &&
            builder->tokens->tokens[builder->index]->content.separator == LISBETH_SYNTHFILESEPARATOR_LBRACKET)
    {
        bool correct = true;
        bool expectComma = false;
        builder->index++;
        while(builder->index < builder->tokens->tokenCount)
        {
            if(builder->tokens->tokens[builder->index]->type == LISBETH_SYNTHFILETOKEN_SEPARATOR)
            {
                if(builder->tokens->tokens[builder->index]->content.separator == LISBETH_SYNTHFILESEPARATOR_RBRACKET)
                {
                    builder->index++;
                    break;
                }
                if(builder->tokens->tokens[builder->index]->content.separator == LISBETH_SYNTHFILESEPARATOR_COMMA)
                {
                    if(!expectComma)
                    {
                        correct = false;
                        break;
                    }
                    builder->index++;
                    expectComma = false;
                    continue;
                }
            }
            if(builder->index + 2 < builder->tokens->tokenCount &&
                    builder->tokens->tokens[builder->index]->type == LISBETH_SYNTHFILETOKEN_SYMBOL &&
                    builder->tokens->tokens[builder->index + 1]->type == LISBETH_SYNTHFILETOKEN_SEPARATOR &&
                    builder->tokens->tokens[builder->index + 1]->content.separator == LISBETH_SYNTHFILESEPARATOR_EQUALS)
            {
                char const *name = builder->tokens->tokens[builder->index]->content.text;
                builder->index += 2;
                Lisbeth_SynthFileExpression *expression = Lisbeth_synthFileBuilder_buildExpression(builder);
                if(!expression)
                {
                    correct = false;
                    break;
                }
                Lisbeth_SynthFileModuleAttribute *attribute = Lisbeth_synthFileModuleAttribute_create(name,expression);
                Lisbeth_synthFileModule_addAttribute(module, attribute);
                expectComma = true;
            }
            else
            {
                correct = false;
                break;
            }
        }
        if(!correct)
        {
            SLog_logger_sendMessage(builder->logger, SLOG_LEVEL_ERROR, "Invalid module attibute. In file \"%s\", line %d", builder->fileName, line);
            Lisbeth_synthFileModule_destroy(module);
            return NULL;
        }
    }
    return module;
}

Lisbeth_SynthFileConnection* Lisbeth_synthFileBuilder_buildConnection(Lisbeth_SynthFileBuilder *builder)
{
    if(!builder)
        return NULL;
    size_t line = builder->tokens->tokens[builder->index]->line;
    if(builder->index + 2 >= builder->tokens->tokenCount)
    {
        SLog_logger_sendMessage(builder->logger, SLOG_LEVEL_ERROR, "Invalide module connection. In file \"%s\", line %d", builder->fileName, line);
        return NULL;
    }
    if(builder->tokens->tokens[builder->index]->type != LISBETH_SYNTHFILETOKEN_SYMBOL ||
            builder->tokens->tokens[builder->index + 1]->type != LISBETH_SYNTHFILETOKEN_SEPARATOR ||
            builder->tokens->tokens[builder->index + 1]->content.separator != LISBETH_SYNTHFILESEPARATOR_COLON ||
            builder->tokens->tokens[builder->index + 2]->type != LISBETH_SYNTHFILETOKEN_SYMBOL)
    {
        SLog_logger_sendMessage(builder->logger, SLOG_LEVEL_ERROR, "Invalid module connection. In file \"%s\", line %d", builder->fileName, line);
        return NULL;
    }
    Lisbeth_SynthFileConnection *connection = Lisbeth_synthFileConnection_create(
            builder->tokens->tokens[builder->index]->content.text,
            builder->tokens->tokens[builder->index + 2]->content.text);
    builder->index += 3;
    return connection;
}

Lisbeth_SynthFileExpression* Lisbeth_synthFileBuilder_buildExpression(Lisbeth_SynthFileBuilder *builder)
{
    if(!builder)
        return NULL;
    if(builder->index >= builder->tokens->tokenCount)
        return NULL;
    size_t line = builder->tokens->tokens[builder->index]->line;
    Lisbeth_SynthFileExpressionFunction **expressionStack = NULL;
    size_t stackSize = 0;
    bool expectComma = false;

    while(true)
    {
        if(builder->index >= builder->tokens->tokenCount)
            goto error;
        switch(builder->tokens->tokens[builder->index]->type)
        {
            case LISBETH_SYNTHFILETOKEN_NUMBER:
            {
                Lisbeth_SynthFileExpression *expression = Lisbeth_synthFileExpression_create(LISBETH_SYNTHFILEEXPR_NUMBER,
                                                                                             &(builder->tokens->tokens[builder->index]->content.number));
                builder->index++;
                if(stackSize == 0)
                    return expression;
                if(expectComma)
                    goto error;
                Lisbeth_synthFileExpressionFunction_addParameter(expressionStack[stackSize - 1], expression);
                expectComma = true;
                break;
            }
            case LISBETH_SYNTHFILETOKEN_STRING:
            {
                Lisbeth_SynthFileExpression *expression = Lisbeth_synthFileExpression_create(LISBETH_SYNTHFILEEXPR_STRING,
                                                                                             builder->tokens->tokens[builder->index]->content.text);
                builder->index++;
                if(stackSize == 0)
                    return expression;
                if(expectComma)
                    goto error;
                Lisbeth_synthFileExpressionFunction_addParameter(expressionStack[stackSize - 1], expression);
                expectComma = true;
                break;
            }
            case LISBETH_SYNTHFILETOKEN_SYMBOL:
            {
                if(builder->index + 1 < builder->tokens->tokenCount &&
                   builder->tokens->tokens[builder->index + 1]->type == LISBETH_SYNTHFILETOKEN_SEPARATOR &&
                   builder->tokens->tokens[builder->index + 1]->content.separator == LISBETH_SYNTHFILESEPARATOR_LPARENTHESIS)
                {
                    Lisbeth_SynthFileExpressionFunction *function = Lisbeth_synthFileExpressionFunction_create(builder->tokens->tokens[builder->index]->content.text);
                    RREALLOC(expressionStack, Lisbeth_SynthFileExpressionFunction*, stackSize + 1);
                    expressionStack[stackSize] = function;
                    stackSize++;
                    builder->index += 2;
                }
                else
                {
                    if(expectComma)
                        goto error;
                    Lisbeth_SynthFileExpression *expression = Lisbeth_synthFileExpression_create(LISBETH_SYNTHFILEEXPR_SYMBOL,
                                                                                                 builder->tokens->tokens[builder->index]->content.text);
                    builder->index++;
                    if(stackSize == 0)
                        return expression;
                    else
                        Lisbeth_synthFileExpressionFunction_addParameter(expressionStack[stackSize - 1], expression);
                    continue;
                }
                break;
            }
            case LISBETH_SYNTHFILETOKEN_SEPARATOR:
            {
                switch(builder->tokens->tokens[builder->index]->content.separator)
                {
                    case LISBETH_SYNTHFILESEPARATOR_COMMA:
                        if(!expectComma)
                            goto error;
                        expectComma = false;
                        builder->index++;
                        break;
                    case LISBETH_SYNTHFILESEPARATOR_RPARENTHESIS:
                        if(stackSize == 0)
                            goto error;
                        builder->index++;
                        if(stackSize == 1)
                        {
                            Lisbeth_SynthFileExpressionFunction *function = expressionStack[0];
                            FFREE(expressionStack);
                            Lisbeth_SynthFileExpression *expression = Lisbeth_synthFileExpression_create(LISBETH_SYNTHFILEEXPR_FUNCTION, function);
                            return expression;
                        }
                        Lisbeth_SynthFileExpression *expression = Lisbeth_synthFileExpression_create(LISBETH_SYNTHFILEEXPR_FUNCTION, expressionStack[stackSize - 1]);
                        Lisbeth_synthFileExpressionFunction_addParameter(expressionStack[stackSize - 2], expression);
                        stackSize--;
                        expectComma = true;
                        break;
                    default:
                        goto error;
                        break;
                }
                break;
            }
            default:
                goto error;
                break;
        }
    }

    return NULL;

    error:
    for(size_t i = 0; i < stackSize; i++)
        Lisbeth_synthFileExpressionFunction_destroy(expressionStack[i]);
    FFREE(expressionStack);
    return NULL;
}


bool Lisbeth_synthFileBuilder_buildBlock(Lisbeth_SynthFileBuilder *builder)
{
    if(!builder)
        return false;
    if(builder->index >= builder->tokens->tokenCount || builder->tokens->tokens[builder->index]->type != LISBETH_SYNTHFILETOKEN_SYMBOL)
        return false;
    size_t line = builder->tokens->tokens[builder->index]->line;
    Lisbeth_SynthFileBlockType type = LISBETH_SYNTHFILEBLOCK_NONE;
    char *commandName = builder->tokens->tokens[builder->index]->content.text;
    for(size_t i = 0; i < LISBETH_SYNTHFILEBLOCK_LAST; i++)
    {
        if(strcmp(Lisbeth_SynthFileKeywords[i], commandName) == 0)
        {
            type = (Lisbeth_SynthFileBlockType)i;
            break;
        }
    }
    if(type == LISBETH_SYNTHFILEBLOCK_NONE)
    {
        SLog_logger_sendMessage(builder->logger, SLOG_LEVEL_ERROR, "Invalid command name \"%s\". In file \"%s\", line %d", commandName, builder->fileName, line);
        return false;
    }
    Lisbeth_SynthFileBlock *block = Lisbeth_synthFileBlock_create(type, builder->tokens->tokens[builder->index]->line, builder->fileName);
    builder->index++;

    while (builder->index < builder->tokens->tokenCount)
    {
        Lisbeth_SynthFileBlockAttribute *attribute = NULL;
        switch (builder->tokens->tokens[builder->index]->type)
        {
            case LISBETH_SYNTHFILETOKEN_SYMBOL:
                if (builder->index + 1 >= builder->tokens->tokenCount)
                {
                    attribute = Lisbeth_synthFileBlockAttribute_create(LISBETH_SYNTHFILEBLOCKATTRIBUTE_SYMBOL,
                                                                       builder->tokens->tokens[builder->index]->content.text);
                    builder->index++;
                }
                else if (builder->tokens->tokens[builder->index + 1]->type == LISBETH_SYNTHFILETOKEN_SEPARATOR &&
                        builder->tokens->tokens[builder->index + 1]->content.separator == LISBETH_SYNTHFILESEPARATOR_LPARENTHESIS)
                {
                    Lisbeth_SynthFileExpression *expression = Lisbeth_synthFileBuilder_buildExpression(builder);
                    if (!expression)
                        goto error;
                    attribute = Lisbeth_synthFileBlockAttribute_create(LISBETH_SYNTHFILEBLOCKATTRIBUTE_EXPRESSION, expression);
                }
                else if (builder->tokens->tokens[builder->index + 1]->type == LISBETH_SYNTHFILETOKEN_SEPARATOR &&
                         builder->tokens->tokens[builder->index + 1]->content.separator == LISBETH_SYNTHFILESEPARATOR_COLON)
                {
                    Lisbeth_SynthFileConnection *connection = Lisbeth_synthFileBuilder_buildConnection(builder);
                    if (!connection)
                        goto error;
                    attribute = Lisbeth_synthFileBlockAttribute_create(LISBETH_SYNTHFILEBLOCKATTRIBUTE_CONNECTION, connection);
                }
                else
                {
                    attribute = Lisbeth_synthFileBlockAttribute_create(LISBETH_SYNTHFILEBLOCKATTRIBUTE_SYMBOL,
                                                                       builder->tokens->tokens[builder->index]->content.text);
                    builder->index++;
                }
                break;
            case LISBETH_SYNTHFILETOKEN_NUMBER:
            {
                Lisbeth_SynthFileExpression *expression = Lisbeth_synthFileBuilder_buildExpression(builder);
                if (!expression)
                    goto error;
                attribute = Lisbeth_synthFileBlockAttribute_create(LISBETH_SYNTHFILEBLOCKATTRIBUTE_EXPRESSION, expression);
            }
                break;
            case LISBETH_SYNTHFILETOKEN_STRING:
                attribute = Lisbeth_synthFileBlockAttribute_create(LISBETH_SYNTHFILEBLOCKATTRIBUTE_STRING,
                                                                   builder->tokens->tokens[builder->index]->content.text);
                builder->index++;
                break;
            case LISBETH_SYNTHFILETOKEN_MODULE:
            {
                Lisbeth_SynthFileModule *module = Lisbeth_synthFileBuilder_buildModule(builder);
                if (!module)
                    goto error;
                attribute = Lisbeth_synthFileBlockAttribute_create(LISBETH_SYNTHFILEBLOCKATTRIBUTE_MODULE, module);
            }
                break;
            case LISBETH_SYNTHFILETOKEN_NEWLINE:
                goto success;
                break;
            default:
                SLog_logger_sendMessage(builder->logger, SLOG_LEVEL_ERROR, "Syntax error. In file \"%s\", line %d",
                        line);
                goto error;
                break;
        }
        Lisbeth_synthFileBlock_addAttribute(block, attribute);
    }

    success:
        Lisbeth_synthFileBlockList_addBlock(builder->blocks, block);
        return true;

    error:
        Lisbeth_synthFileBlock_destroy(block);
        return false;
}
