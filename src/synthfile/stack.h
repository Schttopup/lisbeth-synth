#ifndef LISBETH_SYNTH_STACK_H
#define LISBETH_SYNTH_STACK_H

#include <stdlib.h>


enum Lisbeth_SynthFileStackItemType_
{
    LISBETH_STACK_NONE,
    LISBETH_STACK_ROOT,
    LISBETH_STACK_MACRO,
    LISBETH_STACK_LOOP,
    LISBETH_STACK_COND
};

typedef enum Lisbeth_SynthFileStackItemType_ Lisbeth_SynthFileStackItemType;


struct Lisbeth_SynthFileStackItemRoot_
{
     size_t index;
};

typedef struct Lisbeth_SynthFileStackItemRoot_ Lisbeth_SynthFileStackItemRoot;


struct Lisbeth_SynthFileStackItemMacro_
{
    // TODO
    size_t index;
};

typedef struct Lisbeth_SynthFileStackItemMacro_ Lisbeth_SynthFileStackItemMacro;


struct Lisbeth_SynthFileStackItemLoop_
{
    size_t start;
    size_t end;
    unsigned int steps;
    unsigned int curStep;
    char *name;
    size_t index;
};

typedef struct Lisbeth_SynthFileStackItemLoop_ Lisbeth_SynthFileStackItemLoop;


struct Lisbeth_SynthFileStackItemCond_
{
    size_t index;
};

typedef struct Lisbeth_SynthFileStackItemCond_ Lisbeth_SynthFileStackItemCond;


struct Lisbeth_SynthFileStackItem_
{
    Lisbeth_SynthFileStackItemType type;
    union stack_item_value_
    {
        Lisbeth_SynthFileStackItemRoot *root;
        Lisbeth_SynthFileStackItemMacro *macro;
        Lisbeth_SynthFileStackItemLoop *loop;
        Lisbeth_SynthFileStackItemCond *cond;
    } value;
};

typedef struct Lisbeth_SynthFileStackItem_ Lisbeth_SynthFileStackItem;


struct Lisbeth_SynthFileStack_
{
    Lisbeth_SynthFileStackItem **items;
    size_t count;
};

typedef struct Lisbeth_SynthFileStack_ Lisbeth_SynthFileStack;


Lisbeth_SynthFileStackItemRoot* Lisbeth_synthFileStackItemRoot_create(void);

void Lisbeth_synthFileStackItemRoot_destroy(Lisbeth_SynthFileStackItemRoot *item);

Lisbeth_SynthFileStackItemMacro* Lisbeth_synthFileStackItemMacro_create(void); // TODO

void Lisbeth_synthFileStackItemMacro_destroy(Lisbeth_SynthFileStackItemMacro *item);

Lisbeth_SynthFileStackItemLoop* Lisbeth_synthFileStackItemLoop_create(size_t start, size_t end, unsigned int steps, char *name);

void Lisbeth_synthFileStackItemLoop_destroy(Lisbeth_SynthFileStackItemLoop *item);

Lisbeth_SynthFileStackItemCond* Lisbeth_synthFileStackItemCond_create(size_t index);

void Lisbeth_synthFileStackItemCond_destroy(Lisbeth_SynthFileStackItemCond *item);


Lisbeth_SynthFileStackItem* Lisbeth_synthFileStackItem_create(Lisbeth_SynthFileStackItemType type, void *content);

void Lisbeth_synthFileStackItem_destroy(Lisbeth_SynthFileStackItem *item);

size_t Lisbeth_synthFileStackItem_getIndex(Lisbeth_SynthFileStackItem *item);

void Lisbeth_synthFileStackItem_setIndex(Lisbeth_SynthFileStackItem *item, size_t index);


Lisbeth_SynthFileStack *Lisbeth_synthFileStack_create(void);

void Lisbeth_synthFileStack_destroy(Lisbeth_SynthFileStack *stack);

void Lisbeth_synthFileStack_push(Lisbeth_SynthFileStack *stack, Lisbeth_SynthFileStackItem *item);

void Lisbeth_synthFileStack_pop(Lisbeth_SynthFileStack *stack);

Lisbeth_SynthFileStackItem* Lisbeth_synthFileStack_top(Lisbeth_SynthFileStack *stack);


#endif //LISBETH_SYNTH_STACK_H
