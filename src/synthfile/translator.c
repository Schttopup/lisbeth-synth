#include "translator.h"
#include "translator_cmd.h"
#include <sdefs.h>
#include <sfile/sfile.h>
#include <string.h>


Lisbeth_SynthFileTranslatorFile *Lisbeth_synthFileTranslatorFile_create(Lisbeth_SynthFileBlockList *blocks, char const *name, char const *fileName, char const *moduleName)
{
    if (!blocks || !name || !fileName)
        return NULL;
    Lisbeth_SynthFileTranslatorFile *translatorFile = NULL;
    MMALLOC(translatorFile, Lisbeth_SynthFileTranslatorFile, 1)
    translatorFile->blocks = blocks;
    Lisbeth_SynthModuleGroup *group = Lisbeth_synthModuleGroup_create();
    translatorFile->module = Lisbeth_synthModule_create(name, LISBETH_SYNTHMODULE_GROUP, group);
    translatorFile->stack = Lisbeth_synthFileStack_create();
    Lisbeth_SynthFileStackItemRoot *root = Lisbeth_synthFileStackItemRoot_create();
    Lisbeth_SynthFileStackItem *item = Lisbeth_synthFileStackItem_create(LISBETH_STACK_ROOT, root);
    Lisbeth_synthFileStack_push(translatorFile->stack, item);
    translatorFile->fileName = strdup(fileName);
    if(moduleName)
        translatorFile->moduleName = strdup(moduleName);
    else
        translatorFile->moduleName = NULL;
    return translatorFile;
}

void Lisbeth_synthFileTranslatorFile_destroy(Lisbeth_SynthFileTranslatorFile *file)
{
    if (!file)
        return;
    Lisbeth_synthFileBlockList_destroy(file->blocks);
    FFREE(file->fileName);
    FFREE(file->moduleName);
    Lisbeth_synthFileStack_destroy(file->stack);
    FFREE(file);
}


Lisbeth_SynthFileTranslatorFileStack *Lisbeth_synthFileTranslatorFileStack_create(void)
{
    Lisbeth_SynthFileTranslatorFileStack *stack = NULL;
    MMALLOC(stack, Lisbeth_SynthFileTranslatorFileStack, 1);
    stack->translators = NULL;
    stack->count = 0;
    return stack;
}

void Lisbeth_synthFileTranslatorFileStack_destroy(Lisbeth_SynthFileTranslatorFileStack *stack)
{
    if(!stack)
        return;
    for(size_t i = 0; i < stack->count; i++)
        Lisbeth_synthFileTranslatorFile_destroy(stack->translators[i]);
    FFREE(stack->translators);
    FFREE(stack);
}

void Lisbeth_synthFileTranslatorFileStack_push(Lisbeth_SynthFileTranslatorFileStack *stack, Lisbeth_SynthFileTranslatorFile *file)
{
    if(!stack || !file)
        return;
    RREALLOC(stack->translators, Lisbeth_SynthFileTranslatorFile*, stack->count + 1);
    stack->translators[stack->count] = file;
    stack->count++;
}

void Lisbeth_synthFileTranslatorFileStack_pop(Lisbeth_SynthFileTranslatorFileStack *stack)
{
    if(!stack)
        return;
    if(stack->count == 0)
        return;
    stack->count--;
    Lisbeth_synthFileTranslatorFile_destroy(stack->translators[stack->count]);
    RREALLOC(stack->translators, Lisbeth_SynthFileTranslatorFile*, stack->count);
}

Lisbeth_SynthFileTranslatorFile* Lisbeth_synthFileTranslatorFileStack_top(Lisbeth_SynthFileTranslatorFileStack *stack)
{
    if(!stack)
        return NULL;
    if(stack->count == 0)
        return NULL;
    return stack->translators[stack->count - 1];
}


Lisbeth_SynthFileTranslator* Lisbeth_synthFileTranslator_create(Lisbeth_SynthFileBlockList *blocks, SLog_Logger *logger)
{
    if(!blocks)
        return NULL;
    Lisbeth_SynthFileTranslator *translator = NULL;
    MMALLOC(translator, Lisbeth_SynthFileTranslator, 1);
    translator->stack = Lisbeth_synthFileTranslatorFileStack_create();
    Lisbeth_synthFileTranslatorFileStack_push(translator->stack, Lisbeth_synthFileTranslatorFile_create(blocks, "_ROOT", "_ROOT", NULL));
    translator->source = NULL;
    translator->rootModule = NULL;
    translator->stop = false;
    translator->error = false;
    translator->logger = logger;
    return translator;
}

void Lisbeth_synthFileTranslator_destroy(Lisbeth_SynthFileTranslator *translator)
{
    if(!translator)
        return;
    Lisbeth_synthFileTranslatorFileStack_destroy(translator->stack);
    Lisbeth_synthfileSource_destroy(translator->source);
    FFREE(translator);
}

void Lisbeth_synthFileTranslator_translate(Lisbeth_SynthFileTranslator *translator)
{
    if(!translator)
        return;
    while(!translator->stop && !translator->error)
    {
        Lisbeth_SynthFileTranslatorFile *file = Lisbeth_synthFileTranslatorFileStack_top(translator->stack);
        Lisbeth_SynthFileStackItem *stackTop = Lisbeth_synthFileStack_top(file->stack);
        size_t index = Lisbeth_synthFileStackItem_getIndex(stackTop);
        Lisbeth_synthFileStackItem_setIndex(stackTop, index + 1);
        if(index < file->blocks->blockCount)
        {
            Lisbeth_SynthFileBlock *block = file->blocks->blocks[index];
            SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_DEBUG, "Block [%s]", Lisbeth_SynthFileKeywords[block->type]);
            if(!(*Lisbeth_SynthFileCommandTranslations[block->type])(translator, block))
            {
                translator->error = true;
            }
        }
        else
        {
            Lisbeth_SynthModule *module = file->module;
            Lisbeth_synthFileTranslatorFileStack_pop(translator->stack);
            if(translator->stack->count > 0)
            {
                file = Lisbeth_synthFileTranslatorFileStack_top(translator->stack);
                Lisbeth_synthModuleGroup_addModule(file->module->content.group, module);
            }
            else
            {
                translator->rootModule = module;
                translator->stop = true;
            }
        }
    }
}
