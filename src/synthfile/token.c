#include "token.h"

#include <sdefs.h>
#include <string.h>


char const Lisbeth_SynthFileSeparators[LISBETH_SYNTHFILESEPARATOR_LAST] =
        {
                '\0',
                ':',
                ',',
                '(',
                ')',
                '[',
                ']',
                '='
        };


Lisbeth_SynthFileToken* Lisbeth_synthFileToken_create(Lisbeth_SynthFileTokenType type, void *value, size_t line)
{
    Lisbeth_SynthFileToken *token = NULL;
    MMALLOC(token, Lisbeth_SynthFileToken, 1);
    switch(type)
    {
        case LISBETH_SYNTHFILETOKEN_SYMBOL:
        case LISBETH_SYNTHFILETOKEN_STRING:
        case LISBETH_SYNTHFILETOKEN_MODULE:
            token->content.text = NULL;
            MMALLOC(token->content.text, char, strlen((char*)value) + 1);
            strcpy(token->content.text, (char*)value);
            break;
        case LISBETH_SYNTHFILETOKEN_SEPARATOR:
            token->content.separator = *(Lisbeth_SynthFileSeparator*)value;
            break;
        case LISBETH_SYNTHFILETOKEN_NUMBER:
            token->content.number = *(uint32_t*)value;
            break;
        case LISBETH_SYNTHFILETOKEN_NEWLINE:
            break;
        default:
            FFREE(token);
            return NULL;
    }
    token->type = type;
    token->line = line;
    return token;
}

void Lisbeth_synthFileToken_destroy(Lisbeth_SynthFileToken *token)
{
    if(!token)
        return;
    switch(token->type)
    {
        case LISBETH_SYNTHFILETOKEN_SYMBOL:
        case LISBETH_SYNTHFILETOKEN_STRING:
            FFREE(token->content.text);
            break;
        default:
            break;
    }
    FFREE(token);
}

Lisbeth_SynthFileSeparator Lisbeth_synthFileToken_getSeparator(char c)
{
    for(unsigned int i = 1; i < LISBETH_SYNTHFILESEPARATOR_LAST; i++)
    {
        if(c == Lisbeth_SynthFileSeparators[i])
            return (Lisbeth_SynthFileSeparator)i;
    }
    return LISBETH_SYNTHFILESEPARATOR_NONE;
}


Lisbeth_SynthFileTokenList* Lisbeth_synthFileTokenList_create(void)
{
    Lisbeth_SynthFileTokenList *list = NULL;
    MMALLOC(list, Lisbeth_SynthFileTokenList, 1);
    list->tokens = NULL;
    list->tokenCount = 0;
    return list;
}

void Lisbeth_synthFileTokenList_destroy(Lisbeth_SynthFileTokenList *list)
{
    if(!list)
        return;
    for(size_t i = 0; i < list->tokenCount; i++)
        Lisbeth_synthFileToken_destroy(list->tokens[i]);
    FFREE(list->tokens);
    FFREE(list);
}

void Lisbeth_synthFileTokenList_add(Lisbeth_SynthFileTokenList *list, Lisbeth_SynthFileToken *token)
{
    if(!list || !token)
        return;
    RREALLOC(list->tokens, Lisbeth_SynthFileToken*, list->tokenCount + 1);
    list->tokens[list->tokenCount] = token;
    list->tokenCount++;
}

