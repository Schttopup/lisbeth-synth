#ifndef LISBETH_SYNTH_TRANSLATOR_CMD_H
#define LISBETH_SYNTH_TRANSLATOR_CMD_H

#include "block.h"
#include <stdbool.h>


struct Lisbeth_SynthFileTranslator_;

typedef bool (*Lisbeth_SynthFileCommandTranslation)(struct Lisbeth_SynthFileTranslator_*, Lisbeth_SynthFileBlock*);

extern Lisbeth_SynthFileCommandTranslation const Lisbeth_SynthFileCommandTranslations[LISBETH_SYNTHFILEBLOCK_LAST];


bool Lisbeth_synthFileCommand_module(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block);

bool Lisbeth_synthFileCommand_link(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block);

bool Lisbeth_synthFileCommand_program(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block);

bool Lisbeth_synthFileCommand_bind(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block);

bool Lisbeth_synthFileCommand_expose(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block);

bool Lisbeth_synthFileCommand_attribute(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block);

bool Lisbeth_synthFileCommand_set(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block);

bool Lisbeth_synthFileCommand_import(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block);

bool Lisbeth_synthFileCommand_if(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block);

bool Lisbeth_synthFileCommand_for(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block);

bool Lisbeth_synthFileCommand_end(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block);

bool Lisbeth_synthFileCommand_message(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block);

bool Lisbeth_synthFileCommand_error(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block);


#endif //LISBETH_SYNTH_TRANSLATOR_CMD_H
