#include "parser.h"
#include <sdefs.h>
#include <string.h>


Lisbeth_SynthFileParser* Lisbeth_synthFileParser_create(char const *source, char const *fileName, SLog_Logger *logger)
{
    if(!source)
        return NULL;
    Lisbeth_SynthFileParser *parser = NULL;
    MMALLOC(parser, Lisbeth_SynthFileParser, 1);
    parser->source = source;
    parser->fileName = NULL;
    if(fileName)
    {
        MMALLOC(parser->fileName, char, strlen(fileName) + 1);
        strcpy(parser->fileName, fileName);
    }
    parser->logger = logger;
    parser->tokens = Lisbeth_synthFileTokenList_create();
    parser->index = 0;
    parser->line = 1;
    parser->state = LISBETH_SYNTHPARSER_BLANK;
    return parser;
}

void Lisbeth_synthFileParser_destroy(Lisbeth_SynthFileParser *parser)
{
    if(!parser)
        return;
    FFREE(parser->fileName);
    Lisbeth_synthFileTokenList_destroy(parser->tokens);
    FFREE(parser);
}

Lisbeth_SynthFileTokenList * Lisbeth_synthFile_parse(char const *source, char const *fileName, SLog_Logger *logger)
{
    if(!source)
        return NULL;
    Lisbeth_SynthFileParser *parser = Lisbeth_synthFileParser_create(source, fileName, logger);
    bool stateEnd = true;
    bool stop = false;
    while(*(parser->source + parser->index) != '\0' && !stop)
    {
        if(!stateEnd)
        {
            switch(parser->state)
            {
                case LISBETH_SYNTHPARSER_BLANK:
                    while(Lisbeth_synthFileParser_isWhitespace(*(parser->source + parser->index)))
                        parser->index++;
                    stateEnd = true;
                    break;
                case LISBETH_SYNTHPARSER_SEPARATOR:
                    if(!Lisbeth_synthFileParser_getSeparator(parser))
                    {
                        SLog_logger_sendMessage(logger, SLOG_LEVEL_ERROR,
                                                "Unexpected character '%c'. In file <%s>, line %d",
                                                *(parser->source + parser->index),
                                                parser->fileName,
                                                parser->line);
                        stop = true;
                    }
                    stateEnd = true;
                    break;
                case LISBETH_SYNTHPARSER_SYMBOL:
                    if(!Lisbeth_synthFileParser_getSymbol(parser))
                        stop = true;
                    stateEnd = true;
                    break;
                case LISBETH_SYNTHPARSER_STRING:
                    if(!Lisbeth_synthFileParser_getString(parser))
                        stop = true;
                    stateEnd = true;
                    break;
                case LISBETH_SYNTHPARSER_MODULE:
                    if(!Lisbeth_synthFileParser_getModule(parser))
                        stop = true;
                    stateEnd = true;
                    break;
                case LISBETH_SYNTHPARSER_COMMENT:
                    if(!Lisbeth_synthFileParser_getComment(parser))
                        stop = true;
                    stateEnd = true;
                    break;
                case LISBETH_SYNTHPARSER_NUMBER:
                    if(!Lisbeth_synthFileParser_getNumber(parser))
                        stop = true;
                    stateEnd = true;
                    break;
            }
        }
        else
        {
            char c = parser->source[parser->index];
            if(Lisbeth_synthFileParser_isWhitespace(c))
                parser->state = LISBETH_SYNTHPARSER_BLANK;
            else if(Lisbeth_synthFileParser_isCharacter(c))
                parser->state = LISBETH_SYNTHPARSER_SYMBOL;
            else if(Lisbeth_synthFileParser_isNumber(c))
                parser->state = LISBETH_SYNTHPARSER_NUMBER;
            else if(c == '#')
                parser->state = LISBETH_SYNTHPARSER_COMMENT;
            else if(c == '\"')
            {
                parser->state = LISBETH_SYNTHPARSER_STRING;
                parser->index++;
            }
            else if(c == '<')
            {
                parser->state = LISBETH_SYNTHPARSER_MODULE;
                parser->index++;
            }
            else
                parser->state = LISBETH_SYNTHPARSER_SEPARATOR;
            stateEnd = false;
        }
    }
    Lisbeth_SynthFileTokenList *tokens = parser->tokens;
    parser->tokens = NULL;
    Lisbeth_synthFileParser_destroy(parser);
    return tokens;
}

bool Lisbeth_synthFileParser_isWhitespace(char c)
{
    if(c == ' ' || c == '\t')
        return true;
    return false;
}

bool Lisbeth_synthFileParser_isCharacter(char c)
{
    if(('A' <= c && c <= 'Z') ||
       ('a' <= c && c <= 'z') ||
       c == '_' || c == '$')
        return true;
    return false;
}

bool Lisbeth_synthFileParser_isNumber(char c)
{
    if('0' <= c && c <= '9')
        return true;
    return false;
}

unsigned int Lisbeth_synthFileParser_parseNumber(char const *str, size_t length)
{
    if(!str || !length)
        return 0;
    unsigned int val = 0;
    unsigned int result = 0;
    unsigned int exp = 1;
    const unsigned int base = 10;
    for(int i = length - 1; i >= 0; i--)
    {
        if('0' <= str[i] && str[i] <= '9')
            val = (unsigned int) str[i] - '0';
        else
            return 0;
        result += exp * val;
        exp *= base;
    }
    return result;
}

bool Lisbeth_synthFileParser_getSeparator(Lisbeth_SynthFileParser *parser)
{
    if(!parser)
        return false;
    if(parser->source[parser->index] == '\r' ||
            parser->source[parser->index] == '\n')
        return Lisbeth_synthFileParser_getNewline(parser);
    Lisbeth_SynthFileSeparator separator = Lisbeth_synthFileToken_getSeparator(parser->source[parser->index]);
    if(separator == LISBETH_SYNTHFILESEPARATOR_NONE)
        return false;
    Lisbeth_SynthFileToken *token = Lisbeth_synthFileToken_create(LISBETH_SYNTHFILETOKEN_SEPARATOR, &separator, parser->line);
    Lisbeth_synthFileTokenList_add(parser->tokens, token);
    parser->index++;
    return true;
}

bool Lisbeth_synthFileParser_getSymbol(Lisbeth_SynthFileParser *parser)
{
    if(!parser)
        return false;
    size_t endIndex = parser->index;
    while(Lisbeth_synthFileParser_isCharacter(*(parser->source + endIndex)) ||
          Lisbeth_synthFileParser_isNumber(*(parser->source + endIndex)))
        endIndex++;
    char *tempStr = NULL;
    MMALLOC(tempStr, char, (endIndex - parser->index + 1));
    strncpy(tempStr, parser->source + parser->index, endIndex - parser->index);
    tempStr[endIndex - parser->index] = '\0';
    Lisbeth_SynthFileToken *token = Lisbeth_synthFileToken_create(LISBETH_SYNTHFILETOKEN_SYMBOL, tempStr, parser->line);
    FFREE(tempStr);
    Lisbeth_synthFileTokenList_add(parser->tokens, token);
    parser->index = endIndex;
    return true;
}

bool Lisbeth_synthFileParser_getString(Lisbeth_SynthFileParser *parser)
{
    if(!parser)
        return false;
    unsigned int endIndex = parser->index;
    while(' ' <= parser->source[endIndex] && parser->source[endIndex] <= '~' && parser->source[endIndex] != '\"')
        endIndex++;
    char *tempStr = NULL;
    MMALLOC(tempStr, char, (endIndex - parser->index + 1));
    strncpy(tempStr, parser->source + parser->index, endIndex - parser->index);
    tempStr[endIndex - parser->index] = '\0';
    Lisbeth_SynthFileToken *token = Lisbeth_synthFileToken_create(LISBETH_SYNTHFILETOKEN_STRING, tempStr, parser->line);
    FFREE(tempStr);
    Lisbeth_synthFileTokenList_add(parser->tokens, token);
    parser->index = (*(parser->source + endIndex) == '\"') ? endIndex + 1 : endIndex;
    return true;
}

bool Lisbeth_synthFileParser_getModule(Lisbeth_SynthFileParser *parser)
{
    if(!parser)
        return false;
    unsigned int endIndex = parser->index;
    while(('a' <= parser->source[endIndex] && parser->source[endIndex] <= 'z') ||
            ('A' <= parser->source[endIndex] && parser->source[endIndex] <= 'Z') ||
            ('0' <= parser->source[endIndex] && parser->source[endIndex] <= '9') ||
            parser->source[endIndex] == '/' || parser->source[endIndex] == '_' ||
            parser->source[endIndex] == '-' || parser->source[endIndex] == '.')
        endIndex++;
    if (parser->source[endIndex] != '>')
    {
        SLog_logger_sendMessage(parser->logger, SLOG_LEVEL_ERROR, "Illegal characters in module name. In file \"%s\", line %d",
                parser->fileName, parser->line);
        return false;
    }
    char *tempStr = NULL;
    MMALLOC(tempStr, char, (endIndex - parser->index + 1));
    strncpy(tempStr, parser->source + parser->index, endIndex - parser->index);
    tempStr[endIndex - parser->index] = '\0';
    Lisbeth_SynthFileToken *token = Lisbeth_synthFileToken_create(LISBETH_SYNTHFILETOKEN_MODULE, tempStr, parser->line);
    FFREE(tempStr);
    Lisbeth_synthFileTokenList_add(parser->tokens, token);
    parser->index = (*(parser->source + endIndex) == '>') ? endIndex + 1 : endIndex;
    return true;
}

bool Lisbeth_synthFileParser_getNumber(Lisbeth_SynthFileParser *parser)
{
    if(!parser)
        return false;
    int number = 0;
    unsigned int endIndex = parser->index;
    while(Lisbeth_synthFileParser_isNumber(*(parser->source + endIndex)))
        endIndex++;
    number = Lisbeth_synthFileParser_parseNumber(parser->source + parser->index, endIndex - parser->index);
    Lisbeth_SynthFileToken *token = Lisbeth_synthFileToken_create(LISBETH_SYNTHFILETOKEN_NUMBER, &number, parser->line);
    Lisbeth_synthFileTokenList_add(parser->tokens, token);
    parser->index = endIndex;
    return true;
}

bool Lisbeth_synthFileParser_getComment(Lisbeth_SynthFileParser *parser)
{
    if(!parser)
        return false;
    unsigned int endIndex = parser->index;
    while(*(parser->source + endIndex) != '\0' &&
          *(parser->source + endIndex) != '\n' &&
          *(parser->source + endIndex) != '\r')
        endIndex++;
    parser->index = endIndex;
    return true;
}

bool Lisbeth_synthFileParser_getNewline(Lisbeth_SynthFileParser *parser)
{
    if(!parser)
        return false;
    if(*(parser->source + parser->index) == '\r' ||
            *(parser->source + parser->index) == '\n')
    {
        Lisbeth_SynthFileToken *token = Lisbeth_synthFileToken_create(LISBETH_SYNTHFILETOKEN_NEWLINE, NULL, parser->line);
        Lisbeth_synthFileTokenList_add(parser->tokens, token);
        parser->line++;
        parser->index++;
        if((*(parser->source + parser->index) == '\r' ||
                *(parser->source + parser->index) == '\n') &&
                *(parser->source + parser->index) != *(parser->source + parser->index - 1))
            parser->index++;
        return true;
    }
    return false;
}
