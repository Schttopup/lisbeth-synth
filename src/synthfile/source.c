#include "source.h"
#include <string.h>
#include <sdefs.h>
#include <stdbool.h>
#include <sfile/sfile.h>
#include "parser.h"
#include "builder.h"
#include <sfile/spk.h>


void Lisbeth_synthFileSourceItem_destroy(Lisbeth_SynthFileSourceItem *item)
{
    if(!item)
        return;
    switch(item->type)
    {
        case LISBETH_SOURCEITEM_MODULESCRIPT:
            Lisbeth_synthFileBlockList_destroy(item->content.script.blocks);
            break;
        case LISBETH_SOURCEITEM_MODULEPROGRAM:
            SVExec_memoryObject_destroy(item->content.program.program);
            break;
        case LISBETH_SOURCEITEM_RAWDATA:
            FFREE(item->content.raw.data);
            break;
        default:
            break;
    }
    FFREE(item);
}

Lisbeth_SynthFileSource* Lisbeth_synthFileSource_create(void)
{
    Lisbeth_SynthFileSource *source = NULL;
    MMALLOC(source, Lisbeth_SynthFileSource, 1);
    source->paths = NULL;
    source->pathCount = 0;
    source->items = NULL;
    source->itemCount = 0;
    return source;
}

void Lisbeth_synthfileSource_destroy(Lisbeth_SynthFileSource *source)
{
    if(!source)
        return;
    for(size_t i = 0; i < source->pathCount; i++)
        FFREE(source->paths[i]);
    FFREE(source->paths);
    for(size_t i = 0; i < source->itemCount; i++)
        Lisbeth_synthFileSourceItem_destroy(source->items[i]);
    FFREE(source->items);
    FFREE(source);
}

void Lisbeth_synthFileSource_add(Lisbeth_SynthFileSource *source, char const *path)
{
    if(!source || !path)
        return;
    RREALLOC(source->paths, char*, source->pathCount + 1);
    source->paths[source->pathCount] = SFile_path_absolute(path);
    source->pathCount++;
}

void Lisbeth_synthFileSource_cache(Lisbeth_SynthFileSource *source, Lisbeth_SynthFileSourceItem *item)
{
    if(!source || !item)
        return;
    for(size_t i = 0; i < source->itemCount; i++)
    {
        if(strcmp(item->name, source->items[i]->name) == 0)
        {
            Lisbeth_synthFileSourceItem_destroy(source->items[i]);
            source->items[i] = item;
            return;
        }
    }
    RREALLOC(source->items, Lisbeth_SynthFileSourceItem*, source->itemCount + 1);
    source->items[source->itemCount] = item;
    source->itemCount++;
}

Lisbeth_SynthFileSourceItem* Lisbeth_synthFileSource_load(Lisbeth_SynthFileSource *source, char const *moduleName, char const *fileName)
{
    if(!source || !fileName)
        return NULL;
    char *fullName = NULL;
    if(moduleName != NULL)
        fullName = SFile_path_concat(moduleName, fileName);
    else
        fullName = strdup(fileName);
    for(size_t i = 0; i < source->itemCount; i++)
    {
        if(strcmp(fullName, source->items[i]->name) == 0)
        {
            FFREE(fullName);
            return source->items[i];
        }
    }
    FFREE(fullName);
    if(moduleName != NULL)
    {
        // look in current module for a file
        Lisbeth_SynthFileSourceItem *item = Lisbeth_synthFileSource_loadFile(moduleName, fileName);
        if(item)
        {
            Lisbeth_synthFileSource_cache(source, item);
            return item;
        }
    }
    for(size_t i = 0; i < source->pathCount; i++)
    {
        // look in every source path for a module
        fullName = SFile_path_concat(source->paths[i], fileName);
        Lisbeth_SynthFileSourceItem *item = Lisbeth_synthFileSource_loadFile(fullName, "module.lss");
        if(item)
        {
            Lisbeth_synthFileSource_cache(source, item);
            FFREE(fullName);
            return item;
        }
    }
    return NULL;
}

Lisbeth_SynthFileSourceItem* Lisbeth_synthFileSource_loadFile(char const *moduleName, char const *fileName)
{
    if(!moduleName || !fileName)
        return NULL;
    char *fullName = SFile_path_concat(moduleName, fileName);
    uint8_t *data;
    size_t size;
    bool loaded = SFile_loadRaw(fullName, &data, &size);
    if(!loaded)
        return NULL;
    Lisbeth_SynthFileSourceItem *item = NULL;
    MMALLOC(item, Lisbeth_SynthFileSourceItem, 1);
    item->type = LISBETH_SOURCEITEM_RAWDATA;
    item->content.raw.data = data;
    item->content.raw.size = size;
    item->name = strdup(fileName);
    item->module = strdup(moduleName);
    FFREE(fullName);
    return item;
}
