#include "block.h"
#include <sdefs.h>
#include <string.h>


Lisbeth_SynthFileExpression* Lisbeth_synthFileExpression_create(Lisbeth_SynthFileExpressionType type, void *content)
{
    if(!content)
        return NULL;
    Lisbeth_SynthFileExpression *expression = NULL;
    MMALLOC(expression, Lisbeth_SynthFileExpression, 1);
    expression->type = type;
    switch(type)
    {
        case LISBETH_SYNTHFILEEXPR_NUMBER:
            expression->content.number = *(uint32_t*)(content);
            break;
        case LISBETH_SYNTHFILEEXPR_SYMBOL:
        case LISBETH_SYNTHFILEEXPR_STRING:
            expression->content.text = NULL;
            MMALLOC(expression->content.text, char, strlen((char*)content) + 1);
            strcpy(expression->content.text, (char*)content);
            break;
        case LISBETH_SYNTHFILEEXPR_FUNCTION:
            expression->content.function = (Lisbeth_SynthFileExpressionFunction*)content;
            break;
        default:
            FFREE(expression);
            return NULL;
    }
    return expression;
}

void Lisbeth_synthFileExpression_destroy(Lisbeth_SynthFileExpression *expression)
{
    if(!expression)
        return;
    switch(expression->type)
    {
        case LISBETH_SYNTHFILEEXPR_SYMBOL:
        case LISBETH_SYNTHFILEEXPR_STRING:
            FFREE(expression->content.text);
            break;
        case LISBETH_SYNTHFILEEXPR_FUNCTION:
            Lisbeth_synthFileExpressionFunction_destroy(expression->content.function);
        default:
            break;
    }
    FFREE(expression);
}


Lisbeth_SynthFileExpressionFunction* Lisbeth_synthFileExpressionFunction_create(char const *name)
{
    if(!name)
        return NULL;
    Lisbeth_SynthFileExpressionFunction *function = NULL;
    MMALLOC(function, Lisbeth_SynthFileExpressionFunction, 1);
    function->name = NULL;
    MMALLOC(function->name, char, strlen(name) + 1);
    strcpy(function->name, name);
    function->parameters = NULL;
    function->parameterCount = 0;
    return function;
}

void Lisbeth_synthFileExpressionFunction_destroy(Lisbeth_SynthFileExpressionFunction *function)
{
    if(!function)
        return;
    for(size_t i = 0; i < function->parameterCount; i++)
        Lisbeth_synthFileExpression_destroy(function->parameters[i]);
    FFREE(function->parameters);
    FFREE(function->name);
    FFREE(function);
}

void Lisbeth_synthFileExpressionFunction_addParameter(Lisbeth_SynthFileExpressionFunction *function, Lisbeth_SynthFileExpression *parameter)
{
    if(!function || !parameter)
        return;
    RREALLOC(function->parameters, Lisbeth_SynthFileExpression*, function->parameterCount + 1);
    function->parameters[function->parameterCount] = parameter;
    function->parameterCount++;
}


Lisbeth_SynthFileModuleAttribute* Lisbeth_synthFileModuleAttribute_create(char const *name, Lisbeth_SynthFileExpression *expression)
{
    if(!name || !expression)
        return NULL;
    Lisbeth_SynthFileModuleAttribute *attribute = NULL;
    MMALLOC(attribute, Lisbeth_SynthFileModuleAttribute, 1);
    attribute->name = NULL;
    MMALLOC(attribute->name, char, strlen(name) + 1);
    strcpy(attribute->name, name);
    attribute->expression = expression;
    return attribute;
}

void Lisbeth_synthFileModuleAttribute_destroy(Lisbeth_SynthFileModuleAttribute *attribute)
{
    if(!attribute)
        return;
    FFREE(attribute->name);
    Lisbeth_synthFileExpression_destroy(attribute->expression);
}


Lisbeth_SynthFileModule* Lisbeth_synthFileModule_create(char *name)
{
    if(!name)
        return NULL;
    Lisbeth_SynthFileModule *module = NULL;
    MMALLOC(module, Lisbeth_SynthFileModule, 1);
    module->name = strdup(name);
    module->attributes = NULL;
    module->attributeCount = 0;
    return module;
}

void Lisbeth_synthFileModule_destroy(Lisbeth_SynthFileModule *module)
{
    if(!module)
        return;
    for(size_t i = 0; i < module->attributeCount; i++)
        Lisbeth_synthFileModuleAttribute_destroy(module->attributes[i]);
    FFREE(module->attributes);
    FFREE(module->name);
    FFREE(module);
}

void Lisbeth_synthFileModule_addAttribute(Lisbeth_SynthFileModule *module, Lisbeth_SynthFileModuleAttribute *attribute)
{
    if(!module || !attribute)
        return;
    RREALLOC(module->attributes, Lisbeth_SynthFileModuleAttribute*, module->attributeCount + 1);
    module->attributes[module->attributeCount] = attribute;
    module->attributeCount++;
}


Lisbeth_SynthFileConnection* Lisbeth_synthFileConnection_create(char const *module, char const *port)
{
    if(!module ||!port)
        return NULL;
    Lisbeth_SynthFileConnection *connection = NULL;
    MMALLOC(connection, Lisbeth_SynthFileConnection, 1);
    connection->module = NULL;
    MMALLOC(connection->module, char, strlen(module) + 1);
    strcpy(connection->module, module);
    connection->port = NULL;
    MMALLOC(connection->port, char, strlen(port) + 1);
    strcpy(connection->port, port);
    return connection;
}

void Lisbeth_synthFileConnection_destroy(Lisbeth_SynthFileConnection *connection)
{
    if(!connection)
        return;
    FFREE(connection->module);
    FFREE(connection->port);
    FFREE(connection);
}


Lisbeth_SynthFileBlockAttribute* Lisbeth_synthFileBlockAttribute_create(Lisbeth_SynthFileBlockAttributeType type, void *content)
{
    if(!content)
        return NULL;
    Lisbeth_SynthFileBlockAttribute *attribute = NULL;
    MMALLOC(attribute, Lisbeth_SynthFileBlockAttribute, 1);
    attribute->type = type;
    switch(type)
    {
        case LISBETH_SYNTHFILEBLOCKATTRIBUTE_STRING:
        case LISBETH_SYNTHFILEBLOCKATTRIBUTE_SYMBOL:
            attribute->content.text = NULL;
            MMALLOC(attribute->content.text, char, strlen((char*)content) + 1);
            strcpy(attribute->content.text, (char*)content);
            break;
        case LISBETH_SYNTHFILEBLOCKATTRIBUTE_CONNECTION:
            attribute->content.connection = (Lisbeth_SynthFileConnection*)content;
            break;
        case LISBETH_SYNTHFILEBLOCKATTRIBUTE_EXPRESSION:
            attribute->content.expression = (Lisbeth_SynthFileExpression*)content;
            break;
        case LISBETH_SYNTHFILEBLOCKATTRIBUTE_MODULE:
            attribute->content.module = (Lisbeth_SynthFileModule*)content;
            break;
        default:
            FFREE(attribute);
            return NULL;
            break;
    }
    return attribute;
}

void Lisbeth_synthFileBlockAttribute_destroy(Lisbeth_SynthFileBlockAttribute *attribute)
{
    if(!attribute)
        return;
    switch(attribute->type)
    {
        case LISBETH_SYNTHFILEBLOCKATTRIBUTE_STRING:
        case LISBETH_SYNTHFILEBLOCKATTRIBUTE_SYMBOL:
            FFREE(attribute->content.text);
            break;
        case LISBETH_SYNTHFILEBLOCKATTRIBUTE_CONNECTION:
            Lisbeth_synthFileConnection_destroy(attribute->content.connection);
            break;
        case LISBETH_SYNTHFILEBLOCKATTRIBUTE_EXPRESSION:
            Lisbeth_synthFileExpression_destroy(attribute->content.expression);
            break;
        case LISBETH_SYNTHFILEBLOCKATTRIBUTE_MODULE:
            Lisbeth_synthFileModule_destroy(attribute->content.module);
            break;
        default:
            break;
    }
    FFREE(attribute);
}


char const * const Lisbeth_SynthFileKeywords[LISBETH_SYNTHFILEBLOCK_LAST] =
        {
                "",
                "module",
                "link",
                "program",
                "bind",
                "expose",
                "attribute",
                "set",
                "import",
                "if",
                "for",
                "end",
                "message",
                "error"
        };


Lisbeth_SynthFileBlock* Lisbeth_synthFileBlock_create(Lisbeth_SynthFileBlockType type, size_t line, char const *fileName)
{
    Lisbeth_SynthFileBlock *block = NULL;
    MMALLOC(block, Lisbeth_SynthFileBlock, 1);
    block->type = type;
    block->line = line;
    block->fileName = NULL;
    if(fileName)
    {
        MMALLOC(block->fileName, char, strlen(fileName) + 1);
        strcpy(block->fileName, fileName);
    }
    block->attributes = NULL;
    block->attributeCount = 0;
    return block;
}

void Lisbeth_synthFileBlock_destroy(Lisbeth_SynthFileBlock *block)
{
    if(!block)
        return;
    for(size_t i = 0; i < block->attributeCount; i++)
        Lisbeth_synthFileBlockAttribute_destroy(block->attributes[i]);
    FFREE(block->attributes);
    FFREE(block);
}

void Lisbeth_synthFileBlock_addAttribute(Lisbeth_SynthFileBlock *block, Lisbeth_SynthFileBlockAttribute *attribute)
{
    if(!block || !attribute)
        return;
    RREALLOC(block->attributes, Lisbeth_SynthFileBlockAttribute*, block->attributeCount + 1);
    block->attributes[block->attributeCount] = attribute;
    block->attributeCount++;
}


Lisbeth_SynthFileBlockList* Lisbeth_synthFileBlockList_create(void)
{
    Lisbeth_SynthFileBlockList *list = NULL;
    MMALLOC(list, Lisbeth_SynthFileBlockList, 1);
    list->blocks = NULL;
    list->blockCount = 0;
    return list;
}

void Lisbeth_synthFileBlockList_destroy(Lisbeth_SynthFileBlockList *list)
{
    if(!list)
        return;
    for(size_t i = 0; i < list->blockCount; i++)
        Lisbeth_synthFileBlock_destroy(list->blocks[i]);
    FFREE(list->blocks);
    FFREE(list);
}

void Lisbeth_synthFileBlockList_addBlock(Lisbeth_SynthFileBlockList *list, Lisbeth_SynthFileBlock *block)
{
    if(!list || !block)
        return;
    RREALLOC(list->blocks, Lisbeth_SynthFileBlock*, list->blockCount + 1);
    list->blocks[list->blockCount] = block;
    list->blockCount++;
}
