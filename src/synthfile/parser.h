#ifndef LISBETH_SYNTH_PARSER_H
#define LISBETH_SYNTH_PARSER_H

#include <slog/slog.h>
#include <stdint.h>
#include "token.h"

enum Lisbeth_SynthFileParserState_
{
    LISBETH_SYNTHPARSER_BLANK,
    LISBETH_SYNTHPARSER_SEPARATOR,
    LISBETH_SYNTHPARSER_SYMBOL,
    LISBETH_SYNTHPARSER_STRING,
    LISBETH_SYNTHPARSER_MODULE,
    LISBETH_SYNTHPARSER_NUMBER,
    LISBETH_SYNTHPARSER_COMMENT
};
typedef enum Lisbeth_SynthFileParserState_ Lisbeth_SynthFileParserState;


struct Lisbeth_SynthFileParser_
{
    Lisbeth_SynthFileTokenList *tokens;
    char *fileName;
    SLog_Logger *logger;
    char const *source;
    size_t index;
    size_t line;
    Lisbeth_SynthFileParserState state;
};
typedef struct Lisbeth_SynthFileParser_ Lisbeth_SynthFileParser;


Lisbeth_SynthFileParser* Lisbeth_synthFileParser_create(char const *source, char const *fileName, SLog_Logger *logger);

void Lisbeth_synthFileParser_destroy(Lisbeth_SynthFileParser *parser);

Lisbeth_SynthFileTokenList* Lisbeth_synthFile_parse(char const *source, char const *fileName, SLog_Logger *logger);

bool Lisbeth_synthFileParser_isWhitespace(char c);

bool Lisbeth_synthFileParser_isCharacter(char c);

bool Lisbeth_synthFileParser_isNumber(char c);

bool Lisbeth_synthFileParser_getSeparator(Lisbeth_SynthFileParser *parser);

bool Lisbeth_synthFileParser_getSymbol(Lisbeth_SynthFileParser *parser);

bool Lisbeth_synthFileParser_getString(Lisbeth_SynthFileParser *parser);

bool Lisbeth_synthFileParser_getModule(Lisbeth_SynthFileParser *parser);

bool Lisbeth_synthFileParser_getNumber(Lisbeth_SynthFileParser *parser);

bool Lisbeth_synthFileParser_getComment(Lisbeth_SynthFileParser *parser);

bool Lisbeth_synthFileParser_getNewline(Lisbeth_SynthFileParser *parser);


#endif //LISBETH_SYNTH_PARSER_H
