#ifndef LISBETH_SYNTH_SOURCE_H
#define LISBETH_SYNTH_SOURCE_H

#include <stdlib.h>
#include "block.h"
#include <svexec/memory.h>


enum Lisbeth_SynthFileSourceItemType_
{
    LISBETH_SOURCEITEM_NONE,
    LISBETH_SOURCEITEM_MODULESCRIPT,
    LISBETH_SOURCEITEM_MODULEPROGRAM,
    LISBETH_SOURCEITEM_RAWDATA
};

typedef enum Lisbeth_SynthFileSourceItemType_ Lisbeth_SynthFileSourceItemType;


struct Lisbeth_SynthFileSourceItemScript_
{
    Lisbeth_SynthFileBlockList *blocks;
};
typedef struct Lisbeth_SynthFileSourceItemScript_ Lisbeth_SynthFileSourceItemScript;

struct Lisbeth_SynthFileSourceItemProgram_
{
    SVExec_MemoryObject *program;
};
typedef struct Lisbeth_SynthFileSourceItemProgram_ Lisbeth_SynthFileSourceItemProgram;

struct Lisbeth_SynthFileSourceItemRaw_
{
    uint8_t *data;
    size_t size;
};
typedef struct Lisbeth_SynthFileSourceItemRaw_ Lisbeth_SynthFileSourceItemRaw;


struct Lisbeth_SynthFileSourceItem_
{
    char *name;
    char *module;
    Lisbeth_SynthFileSourceItemType type;
    union
    {
        Lisbeth_SynthFileSourceItemScript script;
        Lisbeth_SynthFileSourceItemProgram program;
        Lisbeth_SynthFileSourceItemRaw raw;
    } content;
};

typedef struct Lisbeth_SynthFileSourceItem_ Lisbeth_SynthFileSourceItem;


struct Lisbeth_SynthFileSource_
{
    char **paths;
    size_t pathCount;
    Lisbeth_SynthFileSourceItem **items;
    size_t itemCount;
};

typedef struct Lisbeth_SynthFileSource_ Lisbeth_SynthFileSource;


void Lisbeth_synthFileSourceItem_destroy(Lisbeth_SynthFileSourceItem *item);

Lisbeth_SynthFileSource* Lisbeth_synthFileSource_create(void);

void Lisbeth_synthfileSource_destroy(Lisbeth_SynthFileSource *source);

void Lisbeth_synthFileSource_add(Lisbeth_SynthFileSource *source, char const *path);

void Lisbeth_synthFileSource_cache(Lisbeth_SynthFileSource *source, Lisbeth_SynthFileSourceItem *item);

Lisbeth_SynthFileSourceItem* Lisbeth_synthFileSource_load(Lisbeth_SynthFileSource *source, char const *moduleName, char const *fileName);

Lisbeth_SynthFileSourceItem* Lisbeth_synthFileSource_loadFile(char const *moduleName, char const *fileName);

#endif //LISBETH_SYNTH_SOURCE_H
