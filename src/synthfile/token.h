#ifndef LISBETH_SYNTH_TOKEN_H
#define LISBETH_SYNTH_TOKEN_H

#include <stdint.h>


enum Lisbeth_SynthFileTokenType_
{
    LISBETH_SYNTHFILETOKEN_NONE,
    LISBETH_SYNTHFILETOKEN_SEPARATOR,
    LISBETH_SYNTHFILETOKEN_NEWLINE,
    LISBETH_SYNTHFILETOKEN_SYMBOL,
    LISBETH_SYNTHFILETOKEN_STRING,
    LISBETH_SYNTHFILETOKEN_MODULE,
    LISBETH_SYNTHFILETOKEN_NUMBER
};
typedef enum Lisbeth_SynthFileTokenType_ Lisbeth_SynthFileTokenType;


enum Lisbeth_SynthFileSeparator_
{
    LISBETH_SYNTHFILESEPARATOR_NONE,
    LISBETH_SYNTHFILESEPARATOR_COLON,
    LISBETH_SYNTHFILESEPARATOR_COMMA,
    LISBETH_SYNTHFILESEPARATOR_LPARENTHESIS,
    LISBETH_SYNTHFILESEPARATOR_RPARENTHESIS,
    LISBETH_SYNTHFILESEPARATOR_LBRACKET,
    LISBETH_SYNTHFILESEPARATOR_RBRACKET,
    LISBETH_SYNTHFILESEPARATOR_EQUALS,
    LISBETH_SYNTHFILESEPARATOR_LAST
};
typedef enum Lisbeth_SynthFileSeparator_ Lisbeth_SynthFileSeparator;

extern char const Lisbeth_SynthFileSeparators[LISBETH_SYNTHFILESEPARATOR_LAST];


struct Lisbeth_SynthFileToken_
{
    Lisbeth_SynthFileTokenType type;
    union
    {
        char *text;
        uint32_t number;
        Lisbeth_SynthFileSeparator separator;
    } content;
    size_t line;
};
typedef struct Lisbeth_SynthFileToken_ Lisbeth_SynthFileToken;


Lisbeth_SynthFileToken* Lisbeth_synthFileToken_create(Lisbeth_SynthFileTokenType type, void *value, size_t line);

void Lisbeth_synthFileToken_destroy(Lisbeth_SynthFileToken *token);

Lisbeth_SynthFileSeparator Lisbeth_synthFileToken_getSeparator(char c);


struct Lisbeth_SynthFileTokenList_
{
    Lisbeth_SynthFileToken **tokens;
    size_t tokenCount;
};
typedef struct Lisbeth_SynthFileTokenList_ Lisbeth_SynthFileTokenList;


Lisbeth_SynthFileTokenList* Lisbeth_synthFileTokenList_create(void);

void Lisbeth_synthFileTokenList_destroy(Lisbeth_SynthFileTokenList *list);

void Lisbeth_synthFileTokenList_add(Lisbeth_SynthFileTokenList *list, Lisbeth_SynthFileToken *token);


#endif //LISBETH_SYNTH_TOKEN_H
