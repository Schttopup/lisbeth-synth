#ifndef LISBETH_SYNTH_TRANSLATOR_H
#define LISBETH_SYNTH_TRANSLATOR_H

#include <stdlib.h>
#include "builder.h"
#include "stack.h"
#include "source.h"
#include "../module.h"
#include "../group.h"
#include <slog/slog.h>


struct Lisbeth_SynthFileTranslatorFile_
{
    char *moduleName;
    char *fileName;
    Lisbeth_SynthFileBlockList *blocks;
    Lisbeth_SynthFileStack *stack;
    Lisbeth_SynthModule *module;
};

typedef struct Lisbeth_SynthFileTranslatorFile_ Lisbeth_SynthFileTranslatorFile;


struct Lisbeth_SynthFileTranslatorFileStack_
{
    Lisbeth_SynthFileTranslatorFile **translators;
    size_t count;
};

typedef struct Lisbeth_SynthFileTranslatorFileStack_ Lisbeth_SynthFileTranslatorFileStack;


struct Lisbeth_SynthFileTranslator_
{
    Lisbeth_SynthModule *rootModule;
    Lisbeth_SynthFileTranslatorFileStack *stack;
    Lisbeth_SynthFileSource *source;
    bool stop;
    bool error;
    SLog_Logger *logger;
};

typedef struct Lisbeth_SynthFileTranslator_ Lisbeth_SynthFileTranslator;


Lisbeth_SynthFileTranslatorFile *Lisbeth_synthFileTranslatorFile_create(Lisbeth_SynthFileBlockList *blocks, char const *name, char const *fileName, char const *moduleName);

void Lisbeth_synthFileTranslatorFile_destroy(Lisbeth_SynthFileTranslatorFile *file);


Lisbeth_SynthFileTranslatorFileStack *Lisbeth_synthFileTranslatorFileStack_create(void);

void Lisbeth_synthFileTranslatorFileStack_destroy(Lisbeth_SynthFileTranslatorFileStack *stack);

void Lisbeth_synthFileTranslatorFileStack_push(Lisbeth_SynthFileTranslatorFileStack *stack, Lisbeth_SynthFileTranslatorFile *file);

void Lisbeth_synthFileTranslatorFileStack_pop(Lisbeth_SynthFileTranslatorFileStack *stack);

Lisbeth_SynthFileTranslatorFile* Lisbeth_synthFileTranslatorFileStack_top(Lisbeth_SynthFileTranslatorFileStack *stack);


Lisbeth_SynthFileTranslator* Lisbeth_synthFileTranslator_create(Lisbeth_SynthFileBlockList *blocks, SLog_Logger *logger);

void Lisbeth_synthFileTranslator_destroy(Lisbeth_SynthFileTranslator *translator);

void Lisbeth_synthFileTranslator_translate(Lisbeth_SynthFileTranslator *translator);


#endif //LISBETH_SYNTH_TRANSLATOR_H
