#include "translator_cmd.h"
#include "translator.h"
#include "../connection.h"
#include "../program.h"
#include <string.h>
#include <svexec/svb.h>


Lisbeth_SynthFileCommandTranslation const Lisbeth_SynthFileCommandTranslations[LISBETH_SYNTHFILEBLOCK_LAST] =
        {
                NULL,
                Lisbeth_synthFileCommand_module,
                Lisbeth_synthFileCommand_link,
                Lisbeth_synthFileCommand_program,
                Lisbeth_synthFileCommand_bind,
                Lisbeth_synthFileCommand_expose,
                Lisbeth_synthFileCommand_attribute,
                Lisbeth_synthFileCommand_set,
                Lisbeth_synthFileCommand_import,
                Lisbeth_synthFileCommand_if,
                Lisbeth_synthFileCommand_for,
                Lisbeth_synthFileCommand_end,
                Lisbeth_synthFileCommand_message,
                Lisbeth_synthFileCommand_error
        };


bool Lisbeth_synthFileCommand_module(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block)
{
    if (!translator || !block)
        return false;
    size_t attrCount = block->attributeCount;
    if (attrCount != 2)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                "Command \"module\" requires 2 attributes, %d provided. In file \"%s\", line %d",
                attrCount, block->fileName, block->line);
        return false;
    }

    if (block->attributes[0]->type != LISBETH_SYNTHFILEBLOCKATTRIBUTE_MODULE)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"module\" requires attribute 1 to be a module. In file \"%s\", line %d",
                                block->fileName, block->line);
        return false;
    }

    if (block->attributes[1]->type != LISBETH_SYNTHFILEBLOCKATTRIBUTE_SYMBOL)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"module\" requires attribute 2 to be a symbol. In file \"%s\", line %d",
                                block->fileName, block->line);
        return false;
    }
    char *moduleSymbol = block->attributes[1]->content.text;
    // TODO expand variables
    Lisbeth_SynthFileModule *fileModule = block->attributes[0]->content.module;
    Lisbeth_SynthFileSourceItem *sourceItem = Lisbeth_synthFileSource_load(translator->source, Lisbeth_synthFileTranslatorFileStack_top(translator->stack)->moduleName, fileModule->name);
    if(!sourceItem)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Module <%s> not found. In file \"%s\", line %d",
                                fileModule->name, block->fileName, block->line);
        return false;
    }
    if(sourceItem->type == LISBETH_SOURCEITEM_RAWDATA)
    {
        char *text = NULL;
        MMALLOC(text, char, sourceItem->content.raw.size + 1);
        memcpy(text, sourceItem->content.raw.data, sourceItem->content.raw.size);
        text[sourceItem->content.raw.size] = '\0';
        Lisbeth_SynthFileTokenList *tokens = Lisbeth_synthFile_parse(text, sourceItem->name, translator->logger);
        FFREE(text);
        if(tokens)
        {
            Lisbeth_SynthFileBlockList *blocks = Lisbeth_synthFileBuilder_build(tokens, sourceItem->name, translator->logger);
            if(blocks)
            {
                sourceItem->type = LISBETH_SOURCEITEM_MODULESCRIPT;
                FFREE(sourceItem->content.raw.data);
                sourceItem->content.script.blocks = blocks;
            }
            Lisbeth_synthFileTokenList_destroy(tokens);
        }
    }
    if(sourceItem->type == LISBETH_SOURCEITEM_MODULESCRIPT)
    {
        Lisbeth_SynthFileTranslatorFile *newFile = Lisbeth_synthFileTranslatorFile_create(sourceItem->content.script.blocks, moduleSymbol, sourceItem->name, sourceItem->module);
        Lisbeth_synthFileTranslatorFileStack_push(translator->stack, newFile);
    }
    else
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                    "Module <%s> is invalid. In file \"%s\", line %d",
                                    fileModule->name, block->fileName, block->line);
        return false;
    }
    return true;
}

bool Lisbeth_synthFileCommand_link(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block)
{
    if (!translator || !block)
        return false;
    size_t attrCount = block->attributeCount;
    if (attrCount != 2)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"link\" requires 2 attributes, %d provided. In file \"%s\", line %d",
                                attrCount, block->fileName, block->line);
        return false;
    }

    if (block->attributes[0]->type != LISBETH_SYNTHFILEBLOCKATTRIBUTE_CONNECTION ||
            block->attributes[1]->type != LISBETH_SYNTHFILEBLOCKATTRIBUTE_CONNECTION)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"link\" requires both attributes to be connections. In file \"%s\", line %d",
                                attrCount, block->fileName, block->line);
        return false;
    }
    // connect
    SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR, "Not implemented : link");
    return false;
}

bool Lisbeth_synthFileCommand_program(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block)
{
    if (!translator || !block)
        return false;
    size_t attrCount = block->attributeCount;
    if (attrCount != 2)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"program\" requires 2 attributes, %d provided. In file \"%s\", line %d",
                                attrCount, block->fileName, block->line);
        return false;
    }

    if (block->attributes[0]->type != LISBETH_SYNTHFILEBLOCKATTRIBUTE_STRING)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"program\" requires attribute 1 to be a string. In file \"%s\", line %d",
                                block->fileName, block->line);
        return false;
    }

    if (block->attributes[1]->type != LISBETH_SYNTHFILEBLOCKATTRIBUTE_SYMBOL)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"program\" requires attribute 2 to be a symbol. In file \"%s\", line %d",
                                block->fileName, block->line);
        return false;
    }
    char *moduleSymbol = block->attributes[1]->content.text;
    // TODO expand variables
    char *fileModule = block->attributes[0]->content.text;
    Lisbeth_SynthFileSourceItem *sourceItem = Lisbeth_synthFileSource_load(translator->source, Lisbeth_synthFileTranslatorFileStack_top(translator->stack)->moduleName, fileModule);
    if(!sourceItem)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "File <%s> not found. In file \"%s\", line %d",
                                fileModule, block->fileName, block->line);
        return false;
    }
    if(sourceItem->type == LISBETH_SOURCEITEM_RAWDATA)
    {

        SVExec_MemoryObject *memory = SVExec_SVB_load(sourceItem->content.raw.data, sourceItem->content.raw.size);
        if(memory)
        {
            sourceItem->type = LISBETH_SOURCEITEM_MODULEPROGRAM;
            FFREE(sourceItem->content.raw.data);
            sourceItem->content.program.program = memory;
        }
    }
    if(sourceItem->type == LISBETH_SOURCEITEM_MODULEPROGRAM)
    {
        static SVExec_SyscallList syscalls; // TODO make this environment-wide
        SVExec_Processor *processor = SVExec_processor_create(&syscalls, sourceItem->content.program.program);
        Lisbeth_SynthModuleProgram *program = Lisbeth_synthModuleProgram_create(processor, sourceItem->content.program.program);
        Lisbeth_SynthModule *programModule = Lisbeth_synthModule_create(moduleSymbol, LISBETH_SYNTHMODULE_BINARY, program);
        Lisbeth_synthModuleGroup_addModule(Lisbeth_synthFileTranslatorFileStack_top(translator->stack)->module->content.group, programModule);
    }
    else
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Program <%s> is invalid. In file \"%s\", line %d",
                                fileModule, block->fileName, block->line);
        return false;
    }
    return true;
}

bool Lisbeth_synthFileCommand_bind(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block)
{
    if (!translator || !block)
        return false;
    size_t attrCount = block->attributeCount;
    if (attrCount != 4)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"bind\" requires 4 attributes, %d provided. In file \"%s\", line %d",
                                attrCount, block->fileName, block->line);
        return false;
    }
    if (block->attributes[0]->type != LISBETH_SYNTHFILEBLOCKATTRIBUTE_CONNECTION)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"bind\" requires attribute 1 to be a connection. In file \"%s\", line %d",
                                block->fileName, block->line);
        return false;
    }
    if (block->attributes[1]->type != LISBETH_SYNTHFILEBLOCKATTRIBUTE_STRING)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"bind\" requires attribute 2 to be a string. In file \"%s\", line %d",
                                block->fileName, block->line);
        return false;
    }
    if (block->attributes[2]->type != LISBETH_SYNTHFILEBLOCKATTRIBUTE_SYMBOL)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"bind\" requires attribute 3 to be a connection type. In file \"%s\", line %d",
                                block->fileName, block->line);
        return false;
    }
    if (block->attributes[3]->type != LISBETH_SYNTHFILEBLOCKATTRIBUTE_SYMBOL)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"bind\" requires attribute 4 to be a connection direction. In file \"%s\", line %d",
                                block->fileName, block->line);
        return false;
    }
    char *moduleName = block->attributes[0]->content.connection->module;
    Lisbeth_SynthModule *module = Lisbeth_synthModuleGroup_getModule(Lisbeth_synthFileTranslatorFileStack_top(translator->stack)->module->content.group,
                                                                     moduleName);
    if(!module)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Module <%s> undeclared. In file \"%s\", line %d",
                                moduleName, block->fileName, block->line);
        return false;
    }
    if(module->type != LISBETH_SYNTHMODULE_BINARY)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Module <%s> is not binary. In file \"%s\", line %d",
                                moduleName, block->fileName, block->line);
        return false;
    }
    char *portName = block->attributes[0]->content.connection->port;
    if(Lisbeth_synthModule_getInputConnection(module, portName) || Lisbeth_synthModule_getOutputConnection(module, portName))
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Module <%s> already has connection <%s>. In file \"%s\", line %d",
                                moduleName, portName, block->fileName, block->line);
        return false;
    }
    Lisbeth_SynthModuleConnectionType connectionType = LISBETH_SOUNDCONNECTION_NONE;
    bool isInput = false;
    char *connectionStr = block->attributes[2]->content.text;
    if(strcmp(connectionStr, "signal") == 0)
        connectionType = LISBETH_SOUNDCONNECTION_SIGNAL;
    else if(strcmp(connectionStr, "value") == 0)
        connectionType = LISBETH_SOUNDCONNECTION_VALUE;
    else if(strcmp(connectionStr, "data") == 0)
        connectionType = LISBETH_SOUNDCONNECTION_DATA;
    else if(strcmp(connectionStr, "readonly") == 0)
        connectionType = LISBETH_SOUNDCONNECTION_READONLY;
    else
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                             "Invalid connection type : \"%s\". In file \"%s\", line %d",
                             connectionStr, block->fileName, block->line);
        return false;
    }
    char *directionStr = block->attributes[3]->content.text;
    if(strcmp(directionStr, "in") == 0)
        isInput = true;
    else if(strcmp(directionStr, "out") == 0)
        isInput = false;
    else
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Invalid connection direction : \"%s\". In file \"%s\", line %d",
                                directionStr, block->fileName, block->line);
        return false;
    }
    char *exportName = block->attributes[1]->content.text;
    uint16_t address = SVExec_exportList_get(module->content.program->memory->exports, exportName);
    uint8_t *addressPtr = SVExec_processor_getMemory(module->content.program->processor, address, Lisbeth_SynthModuleConnectionSizes[connectionType]);
    if(!addressPtr)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Invalid export : \"%s\". In file \"%s\", line %d",
                                exportName, block->fileName, block->line);
        return false;
    }
    Lisbeth_SynthModuleConnection *connection = Lisbeth_synthModuleConnection_create(portName, connectionType, addressPtr);
    if(isInput)
        Lisbeth_synthModule_addInputConnection(module, connection);
    else
        Lisbeth_synthModule_addOutputConnection(module, connection);
    return true;
}

bool Lisbeth_synthFileCommand_expose(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block)
{
    if (!translator || !block)
        return false;
    size_t attrCount = block->attributeCount;
    if (attrCount != 2)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"expose\" requires 2 attributes, %d provided. In file \"%s\", line %d",
                                attrCount, block->fileName, block->line);
        return false;
    }
    if (block->attributes[0]->type != LISBETH_SYNTHFILEBLOCKATTRIBUTE_CONNECTION)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"bind\" requires attribute 1 to be a connection. In file \"%s\", line %d",
                                block->fileName, block->line);
        return false;
    }
    if (block->attributes[1]->type != LISBETH_SYNTHFILEBLOCKATTRIBUTE_SYMBOL)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"bind\" requires attribute 2 to be a symbol. In file \"%s\", line %d",
                                block->fileName, block->line);
        return false;
    }
    char *exposedName = block->attributes[1]->content.text;
    Lisbeth_SynthModule *topModule = Lisbeth_synthFileTranslatorFileStack_top(translator->stack)->module;
    if(Lisbeth_synthModule_getInputConnection(topModule, exposedName) || Lisbeth_synthModule_getOutputConnection(topModule, exposedName))
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Connection <%s> is already exposed. In file \"%s\", line %d",
                                exposedName, block->fileName, block->line);
        return false;
    }
    char *moduleName = block->attributes[0]->content.connection->module;
    Lisbeth_SynthModule *module = Lisbeth_synthModuleGroup_getModule(Lisbeth_synthFileTranslatorFileStack_top(translator->stack)->module->content.group,
                                                                     moduleName);
    if(!module)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Module <%s> undeclared. In file \"%s\", line %d",
                                moduleName, block->fileName, block->line);
        return false;
    }

    char *portName = block->attributes[0]->content.connection->port;
    Lisbeth_SynthModuleConnection *connection = NULL;
    if((connection = Lisbeth_synthModule_getInputConnection(module, portName)))
    {
        for(size_t i = 0; i < topModule->inputCount; i++)
        {
            if(topModule->inputs[i]->internal == connection)
            {
                SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                     "Connection <%s:%s> is already exposed. In file \"%s\", line %d",
                                     moduleName, portName, block->fileName, block->line);
                return false;
            }
        }
        Lisbeth_SynthModuleConnection *exposedConnection = Lisbeth_synthModuleConnection_create(exposedName, connection->type, connection->self);
        exposedConnection->internal = connection;
        Lisbeth_synthModule_addInputConnection(topModule, exposedConnection);
    }
    else if((connection = Lisbeth_synthModule_getOutputConnection(module, portName)))
    {
        for(size_t i = 0; i < topModule->outputCount; i++)
        {
            if(topModule->outputs[i]->internal == connection)
            {
                SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                        "Connection <%s:%s> is already exposed. In file \"%s\", line %d",
                                        moduleName, portName, block->fileName, block->line);
                return false;
            }
        }
        Lisbeth_SynthModuleConnection *exposedConnection = Lisbeth_synthModuleConnection_create(exposedName, connection->type, connection->self);
        exposedConnection->internal = connection;
        Lisbeth_synthModule_addOutputConnection(topModule, exposedConnection);
    }
    else
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Connection <%s:%s> undeclared. In file \"%s\", line %d",
                                moduleName, portName, block->fileName, block->line);
        return false;
    }
    return true;
}

bool Lisbeth_synthFileCommand_attribute(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block)
{
    if (!translator || !block)
        return false;
    size_t attrCount = block->attributeCount;
    SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR, "Not implemented : attribute");
    return true;
}

bool Lisbeth_synthFileCommand_set(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block)
{
    if (!translator || !block)
        return false;
    size_t attrCount = block->attributeCount;
    SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR, "Not implemented : set");
    return true;
}

bool Lisbeth_synthFileCommand_import(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block)
{
    if (!translator || !block)
        return false;
    size_t attrCount = block->attributeCount;
    SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR, "Not implemented : import");
    return true;
}

bool Lisbeth_synthFileCommand_if(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block)
{
    if (!translator || !block)
        return false;
    size_t attrCount = block->attributeCount;
    SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR, "Not implemented : if");
    return true;
}

bool Lisbeth_synthFileCommand_for(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block)
{
    if (!translator || !block)
        return false;
    size_t attrCount = block->attributeCount;
    SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR, "Not implemented : for");
    return true;
}

bool Lisbeth_synthFileCommand_end(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block)
{
    if (!translator || !block)
        return false;
    size_t attrCount = block->attributeCount;
    SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR, "Not implemented : end");
    return true;
}

bool Lisbeth_synthFileCommand_message(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block)
{
    if (!translator || !block)
        return false;
    size_t attrCount = block->attributeCount;
    if (attrCount != 1)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"message\" requires 1 attribute, %d provided. In file \"%s\", line %d",
                                attrCount, block->fileName, block->line);
        return false;
    }
    if (block->attributes[0]->type != LISBETH_SYNTHFILEBLOCKATTRIBUTE_STRING)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"message\" requires attribute 1 to be a string. In file \"%s\", line %d",
                                attrCount, block->fileName, block->line);
        return false;
    }
    Lisbeth_SynthFileTranslatorFile *file = Lisbeth_synthFileTranslatorFileStack_top(translator->stack);
    if(file->moduleName != NULL)
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_INFO, "%s:%s > %s", file->moduleName, file->fileName, block->attributes[0]->content.text);
    else
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_INFO, "%s > %s", file->fileName, block->attributes[0]->content.text);
    return true;
}

bool Lisbeth_synthFileCommand_error(struct Lisbeth_SynthFileTranslator_ *translator, Lisbeth_SynthFileBlock *block)
{
    if (!translator || !block)
        return false;
    size_t attrCount = block->attributeCount;
    if (attrCount != 1)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"error\" requires 1 attribute, %d provided. In file \"%s\", line %d",
                                attrCount, block->fileName, block->line);
        return false;
    }
    if (block->attributes[0]->type != LISBETH_SYNTHFILEBLOCKATTRIBUTE_STRING)
    {
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR,
                                "Command \"error\" requires attribute 1 to be a string. In file \"%s\", line %d",
                                attrCount, block->fileName, block->line);
        return false;
    }
    Lisbeth_SynthFileTranslatorFile *file = Lisbeth_synthFileTranslatorFileStack_top(translator->stack);
    if(file->moduleName != NULL)
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR, "%s:%s > %s", file->moduleName, file->fileName, block->attributes[0]->content.text);
    else
        SLog_logger_sendMessage(translator->logger, SLOG_LEVEL_ERROR, "%s > %s", file->fileName, block->attributes[0]->content.text);
    return false;
}

