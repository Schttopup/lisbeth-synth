#ifndef LISBETH_SYNTH_BUILDER_H
#define LISBETH_SYNTH_BUILDER_H

#include <slog/slog.h>
#include <stdint.h>
#include <stdbool.h>
#include "parser.h"
#include "block.h"


struct Lisbeth_SynthFileBuilder_
{
    Lisbeth_SynthFileBlockList *blocks;
    Lisbeth_SynthFileTokenList *tokens;
    size_t index;
    char *fileName;
    SLog_Logger *logger;
};
typedef struct Lisbeth_SynthFileBuilder_ Lisbeth_SynthFileBuilder;


Lisbeth_SynthFileBuilder* Lisbeth_synthFileBuilder_create(Lisbeth_SynthFileTokenList *tokens, char const *fileName , SLog_Logger *logger);

void Lisbeth_synthFileBuiler_destroy(Lisbeth_SynthFileBuilder *builder);


Lisbeth_SynthFileBlockList* Lisbeth_synthFileBuilder_build(Lisbeth_SynthFileTokenList *tokens, char const *fileName, SLog_Logger *logger);


void Lisbeth_synthFileBuilder_nextLine(Lisbeth_SynthFileBuilder *builder);

Lisbeth_SynthFileModule* Lisbeth_synthFileBuilder_buildModule(Lisbeth_SynthFileBuilder *builder);

Lisbeth_SynthFileConnection* Lisbeth_synthFileBuilder_buildConnection(Lisbeth_SynthFileBuilder *builder);

Lisbeth_SynthFileExpression* Lisbeth_synthFileBuilder_buildExpression(Lisbeth_SynthFileBuilder *builder);


bool Lisbeth_synthFileBuilder_buildBlock(Lisbeth_SynthFileBuilder *builder);


#endif //LISBETH_SYNTH_BUILDER_H
