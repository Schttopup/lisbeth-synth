#ifndef LISBETH_SYNTH_BLOCK_H
#define LISBETH_SYNTH_BLOCK_H


#include <stdint.h>
#include "token.h"


enum Lisbeth_SynthFileExpressionType_
{
    LISBETH_SYNTHFILEEXPR_NONE,
    LISBETH_SYNTHFILEEXPR_NUMBER,
    LISBETH_SYNTHFILEEXPR_STRING,
    LISBETH_SYNTHFILEEXPR_SYMBOL,
    LISBETH_SYNTHFILEEXPR_FUNCTION
};
typedef enum Lisbeth_SynthFileExpressionType_ Lisbeth_SynthFileExpressionType;

struct Lisbeth_SynthFileExpressionFunction_;

struct Lisbeth_SynthFileExpression_
{
    Lisbeth_SynthFileExpressionType type;
    union
    {
        uint32_t number;
        char *text;
        struct Lisbeth_SynthFileExpressionFunction_ *function;
    } content;
};
typedef struct Lisbeth_SynthFileExpression_ Lisbeth_SynthFileExpression;


Lisbeth_SynthFileExpression* Lisbeth_synthFileExpression_create(Lisbeth_SynthFileExpressionType type, void *content);

void Lisbeth_synthFileExpression_destroy(Lisbeth_SynthFileExpression *expression);


struct Lisbeth_SynthFileExpressionFunction_
{
    char *name;
    Lisbeth_SynthFileExpression **parameters;
    size_t parameterCount;
};
typedef struct Lisbeth_SynthFileExpressionFunction_ Lisbeth_SynthFileExpressionFunction;


Lisbeth_SynthFileExpressionFunction* Lisbeth_synthFileExpressionFunction_create(char const *name);

void Lisbeth_synthFileExpressionFunction_destroy(Lisbeth_SynthFileExpressionFunction *function);

void Lisbeth_synthFileExpressionFunction_addParameter(Lisbeth_SynthFileExpressionFunction *function, Lisbeth_SynthFileExpression *parameter);


struct Lisbeth_SynthFileModuleAttribute_
{
    char *name;
    Lisbeth_SynthFileExpression *expression;
};
typedef struct Lisbeth_SynthFileModuleAttribute_ Lisbeth_SynthFileModuleAttribute;


Lisbeth_SynthFileModuleAttribute* Lisbeth_synthFileModuleAttribute_create(char const *name, Lisbeth_SynthFileExpression *expression);

void Lisbeth_synthFileModuleAttribute_destroy(Lisbeth_SynthFileModuleAttribute *attribute);


struct Lisbeth_SynthFileModule_
{
    char *name;
    Lisbeth_SynthFileModuleAttribute **attributes;
    size_t attributeCount;
};
typedef struct Lisbeth_SynthFileModule_ Lisbeth_SynthFileModule;


Lisbeth_SynthFileModule* Lisbeth_synthFileModule_create(char *name);

void Lisbeth_synthFileModule_destroy(Lisbeth_SynthFileModule *module);

void Lisbeth_synthFileModule_addAttribute(Lisbeth_SynthFileModule *module, Lisbeth_SynthFileModuleAttribute *attribute);


struct Lisbeth_SynthFileConnection_
{
    char *module;
    char *port;
};
typedef struct Lisbeth_SynthFileConnection_ Lisbeth_SynthFileConnection;


Lisbeth_SynthFileConnection* Lisbeth_synthFileConnection_create(char const *module, char const *port);

void Lisbeth_synthFileConnection_destroy(Lisbeth_SynthFileConnection *connection);


enum Lisbeth_SynthFileBlockAttributeType_
{
    LISBETH_SYNTHFILEBLOCKATTRIBUTE_NONE,
    LISBETH_SYNTHFILEBLOCKATTRIBUTE_SYMBOL,
    LISBETH_SYNTHFILEBLOCKATTRIBUTE_STRING,
    LISBETH_SYNTHFILEBLOCKATTRIBUTE_EXPRESSION,
    LISBETH_SYNTHFILEBLOCKATTRIBUTE_MODULE,
    LISBETH_SYNTHFILEBLOCKATTRIBUTE_CONNECTION
};
typedef enum Lisbeth_SynthFileBlockAttributeType_ Lisbeth_SynthFileBlockAttributeType;


struct Lisbeth_SynthFileBlockAttribute_
{
    Lisbeth_SynthFileBlockAttributeType type;
    union
    {
        char *text;
        Lisbeth_SynthFileExpression *expression;
        Lisbeth_SynthFileModule *module;
        Lisbeth_SynthFileConnection *connection;
    } content;
};
typedef struct Lisbeth_SynthFileBlockAttribute_ Lisbeth_SynthFileBlockAttribute;


Lisbeth_SynthFileBlockAttribute* Lisbeth_synthFileBlockAttribute_create(Lisbeth_SynthFileBlockAttributeType type, void *content);

void Lisbeth_synthFileBlockAttribute_destroy(Lisbeth_SynthFileBlockAttribute *attribute);


enum Lisbeth_SynthFileBlockType_
{
    LISBETH_SYNTHFILEBLOCK_NONE,
    LISBETH_SYNTHFILEBLOCK_MODULE,
    LISBETH_SYNTHFILEBLOCK_LINK,
    LISBETH_SYNTHFILEBLOCK_PROGRAM,
    LISBETH_SYNTHFILEBLOCK_BIND,
    LISBETH_SYNTHFILEBLOCK_EXPOSE,
    LISBETH_SYNTHFILEBLOCK_ATTRIBUTE,
    LISBETH_SYNTHFILEBLOCK_SET,
    LISBETH_SYNTHFILEBLOCK_IMPORT,
    LISBETH_SYNTHFILEBLOCK_IF,
    LISBETH_SYNTHFILEBLOCK_FOR,
    LISBETH_SYNTHFILEBLOCK_END,
    LISBETH_SYNTHFILEBLOCK_MESSAGE,
    LISBETH_SYNTHFILEBLOCK_ERROR,
    LISBETH_SYNTHFILEBLOCK_LAST
};
typedef enum Lisbeth_SynthFileBlockType_ Lisbeth_SynthFileBlockType;

extern char const * const Lisbeth_SynthFileKeywords[LISBETH_SYNTHFILEBLOCK_LAST];


struct Lisbeth_SynthFileBlock_
{
    Lisbeth_SynthFileBlockType type;
    Lisbeth_SynthFileBlockAttribute **attributes;
    size_t attributeCount;
    size_t line;
    char *fileName;
};
typedef struct Lisbeth_SynthFileBlock_ Lisbeth_SynthFileBlock;


Lisbeth_SynthFileBlock* Lisbeth_synthFileBlock_create(Lisbeth_SynthFileBlockType type, size_t line, char const *fileName);

void Lisbeth_synthFileBlock_destroy(Lisbeth_SynthFileBlock *block);

void Lisbeth_synthFileBlock_addAttribute(Lisbeth_SynthFileBlock *block, Lisbeth_SynthFileBlockAttribute *attribute);

Lisbeth_SynthFileBlock* Lisbeth_synthFileBlock_copy(Lisbeth_SynthFileBlock *block);


struct Lisbeth_SynthFileBlockList_
{
    Lisbeth_SynthFileBlock **blocks;
    size_t blockCount;
};
typedef struct Lisbeth_SynthFileBlockList_ Lisbeth_SynthFileBlockList;


Lisbeth_SynthFileBlockList* Lisbeth_synthFileBlockList_create(void);

void Lisbeth_synthFileBlockList_destroy(Lisbeth_SynthFileBlockList *list);

void Lisbeth_synthFileBlockList_addBlock(Lisbeth_SynthFileBlockList *list, Lisbeth_SynthFileBlock *block);

Lisbeth_SynthFileBlockList* Lisbeth_synthFileBlockList_copy(Lisbeth_SynthFileBlockList *list);


#endif //LISBETH_SYNTH_BLOCK_H
