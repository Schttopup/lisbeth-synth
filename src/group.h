#ifndef LISBETH_SYNTH_GROUP_H
#define LISBETH_SYNTH_GROUP_H

#include <stdlib.h>
#include "module.h"


struct Lisbeth_SynthModuleGroup_
{
    struct Lisbeth_SynthModule_ **modules;
    size_t moduleCount;
    // TODO priority order here
};
typedef struct Lisbeth_SynthModuleGroup_ Lisbeth_SynthModuleGroup;


Lisbeth_SynthModuleGroup* Lisbeth_synthModuleGroup_create(void);

void Lisbeth_synthModuleGroup_destroy(Lisbeth_SynthModuleGroup *group);

void Lisbeth_synthModuleGroup_addModule(Lisbeth_SynthModuleGroup *group, Lisbeth_SynthModule *module);

Lisbeth_SynthModule* Lisbeth_synthModuleGroup_getModule(Lisbeth_SynthModuleGroup *group, char *name);

void Lisbeth_synthModuleGroup_computeOrder(Lisbeth_SynthModuleGroup *group);


#endif //LISBETH_SYNTH_GROUP_H
