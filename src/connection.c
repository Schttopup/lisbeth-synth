#include "connection.h"
#include <string.h>


size_t Lisbeth_SynthModuleConnectionSizes[] =
        {
                [LISBETH_SOUNDCONNECTION_NONE] =      0,
                [LISBETH_SOUNDCONNECTION_SIGNAL] =    sizeof(double),
                [LISBETH_SOUNDCONNECTION_VALUE] =     sizeof(uint64_t),
                [LISBETH_SOUNDCONNECTION_DATA] =      sizeof(uint8_t) * 8,
                [LISBETH_SOUNDCONNECTION_READONLY] =  sizeof(uint8_t) * 8
        };


Lisbeth_SynthModuleConnection *Lisbeth_synthModuleConnection_create(char const *name, Lisbeth_SynthModuleConnectionType type, uint8_t *self)
{
    if(!name || !self)
        return NULL;
    if(type <= LISBETH_SOUNDCONNECTION_NONE || type >= LISBETH_SOUNDCONNECTION_LAST)
        return NULL;
    Lisbeth_SynthModuleConnection *connection = NULL;
    MMALLOC(connection, Lisbeth_SynthModuleConnection, 1);
    connection->name = NULL;
    MMALLOC(connection->name, char, strlen(name) + 1);
    strcpy(connection->name, name);
    connection->type = type;
    connection->internal = NULL;
    connection->self = self;
    connection->buffer = NULL;
    connection->bufferSize = 0;
    connection->bufferOwner = false;
    connection->index = 0;
    return connection;
}

void Lisbeth_synthModuleConnection_destroy(Lisbeth_SynthModuleConnection *connection)
{
    if(!connection)
        return;
    FFREE(connection->name);
    if(connection->bufferOwner)
        FFREE(connection->buffer);
    FFREE(connection);
}

bool Lisbeth_synthModule_connect(Lisbeth_SynthModule *output, char const *outputName, Lisbeth_SynthModule *input, char const *inputName)
{
    if(!output || !input || !outputName || !inputName)
        return false;
    Lisbeth_SynthModuleConnection *outputConnection = NULL;
    Lisbeth_SynthModuleConnection *inputConnection = NULL;
    for(size_t i = 0; i < output->outputCount; i++)
    {
        if(strcmp(outputName, output->outputs[i]->name) == 0)
        {
            outputConnection = output->outputs[i];
            break;
        }
    }
    if(!outputConnection)
        return false;
    while(outputConnection->internal)
        outputConnection = outputConnection->internal;
    for(size_t i = 0; i < input->inputCount; i++)
    {
        if(strcmp(inputName, input->inputs[i]->name) == 0)
        {
            inputConnection = input->inputs[i];
            break;
        }
    }
    if(!inputConnection)
        return false;
    while(inputConnection->internal)
        inputConnection = outputConnection->internal;
    return true;
}
