#include "group.h"
#include <string.h>


Lisbeth_SynthModuleGroup* Lisbeth_synthModuleGroup_create(void)
{
    Lisbeth_SynthModuleGroup *group = NULL;
    MMALLOC(group, Lisbeth_SynthModuleGroup, 1);
    group->moduleCount = 0;
    group->modules = NULL;
    return group;
}

void Lisbeth_synthModuleGroup_destroy(Lisbeth_SynthModuleGroup *group)
{
    if(!group)
        return;
    for(size_t i = 0; i < group->moduleCount; i++)
        Lisbeth_synthModule_destroy(group->modules[i]);
    FFREE(group->modules);
    FFREE(group);
}

void Lisbeth_synthModuleGroup_addModule(Lisbeth_SynthModuleGroup *group, Lisbeth_SynthModule *module)
{
    if(!group || !module)
        return;
    RREALLOC(group->modules, Lisbeth_SynthModule*, group->moduleCount + 1);
    group->modules[group->moduleCount] = module;
    group->moduleCount++;
}

Lisbeth_SynthModule* Lisbeth_synthModuleGroup_getModule(Lisbeth_SynthModuleGroup *group, char *name)
{
    if(!group || !name)
        return NULL;
    for(size_t i = 0; i < group->moduleCount; i++)
    {
        if(strcmp(group->modules[i]->name, name) == 0)
            return group->modules[i];
    }
    return NULL;
}

void Lisbeth_synthModuleGroup_computeOrder(Lisbeth_SynthModuleGroup *group)
{
    UNUSED(group);
    // TODO
}

