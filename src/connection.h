#ifndef LISBETH_SYNTH_CONNECTION_H
#define LISBETH_SYNTH_CONNECTION_H

#include <stdint.h>
#include <stdlib.h>
#include "module.h"


enum Lisbeth_SynthModuleConnectionType_
{
    LISBETH_SOUNDCONNECTION_NONE,
    LISBETH_SOUNDCONNECTION_SIGNAL,
    LISBETH_SOUNDCONNECTION_VALUE,
    LISBETH_SOUNDCONNECTION_DATA,
    LISBETH_SOUNDCONNECTION_READONLY,
    LISBETH_SOUNDCONNECTION_LAST
};
typedef enum Lisbeth_SynthModuleConnectionType_ Lisbeth_SynthModuleConnectionType;

extern size_t Lisbeth_SynthModuleConnectionSizes[LISBETH_SOUNDCONNECTION_LAST];


struct Lisbeth_SynthModuleConnection_
{
    char *name;         //!< Connection name
    Lisbeth_SynthModuleConnectionType type;
    struct Lisbeth_SynthModuleConnection_ *internal;    //!< Internal connection exposition. If this is not null, other pointers are ignored.
    uint8_t *self;      //!< Pointer to self memory area, regardless of input or output
    uint8_t *buffer;    //!< Buffer to data. Points to actual remote memory area if size is 1
    size_t bufferSize;  //!< Buffer size in samples
    bool bufferOwner;   //!< Buffer owner for memory management. Owner is always the module output
    size_t index;       //!< R/W index TODO
};
typedef struct Lisbeth_SynthModuleConnection_ Lisbeth_SynthModuleConnection;


Lisbeth_SynthModuleConnection *Lisbeth_synthModuleConnection_create(char const *name, Lisbeth_SynthModuleConnectionType type, uint8_t *self);

void Lisbeth_synthModuleConnection_destroy(Lisbeth_SynthModuleConnection *connection);

bool Lisbeth_synthModule_connect(Lisbeth_SynthModule *output, char const *outputName, Lisbeth_SynthModule *input, char const *inputName);
// TODO refactor that


#endif //LISBETH_SYNTH_CONNECTION_H
