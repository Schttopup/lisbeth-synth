#include <stdio.h>
#include <sdefs.h>
#include <sfile/sfile.h>
#include <slog/slog.h>
#include "synthfile/parser.h"
#include "synthfile/builder.h"
#include "synthfile/translator.h"
#include "synthfile/source.h"

void print_token(Lisbeth_SynthFileToken *token)
{
    if(!token)
        return;
    int line = token->line;
    switch(token->type)
    {
        case LISBETH_SYNTHFILETOKEN_SEPARATOR:
            printf("%d\tSeparator %c\n", line, Lisbeth_SynthFileSeparators[token->content.separator]);
            break;
        case LISBETH_SYNTHFILETOKEN_NUMBER:
            printf("%d\tNumber %d\n", line, token->content.number);
            break;
        case LISBETH_SYNTHFILETOKEN_SYMBOL:
            printf("%d\tSymbol %s\n", line, token->content.text);
            break;
        case LISBETH_SYNTHFILETOKEN_STRING:
            printf("%d\tString \"%s\"\n", line, token->content.text);
            break;
        case LISBETH_SYNTHFILETOKEN_NEWLINE:
            printf("%d\t---\n", line);
        default:
            break;
    }
}

void print_expr(Lisbeth_SynthFileExpression *expr)
{
    if(!expr)
        return;
    switch(expr->type)
    {
        case LISBETH_SYNTHFILEEXPR_NUMBER:
            printf("%d", expr->content.number);
            break;
        case LISBETH_SYNTHFILEEXPR_SYMBOL:
            printf("%s", expr->content.text);
            break;
        case LISBETH_SYNTHFILEEXPR_STRING:
            printf("\"%s\"", expr->content.text);
            break;
        case LISBETH_SYNTHFILEEXPR_FUNCTION:
            printf("%s(", expr->content.function->name);
            for(size_t i = 0; i < expr->content.function->parameterCount; i++)
            {
                printf(" ");
                print_expr(expr->content.function->parameters[i]);
            }
            printf(" )");
            break;
        default:
            break;
    }
}

void print_block(Lisbeth_SynthFileBlock *block)
{
    if(!block)
        return;
    printf("%d\t%s", (int)block->line, Lisbeth_SynthFileKeywords[block->type]);
    for(size_t i = 0; i < block->attributeCount; i++)
    {
        Lisbeth_SynthFileBlockAttribute *attr = block->attributes[i];
        switch(attr->type)
        {
            case LISBETH_SYNTHFILEBLOCKATTRIBUTE_SYMBOL:
                printf(" - <%s>", attr->content.text);
                break;
            case LISBETH_SYNTHFILEBLOCKATTRIBUTE_STRING:
                printf(" - \"%s\"", attr->content.text);
                break;
            case LISBETH_SYNTHFILEBLOCKATTRIBUTE_MODULE:
                printf(" - %s[", attr->content.module->name);
                for(size_t j = 0; j < attr->content.module->attributeCount; j++)
                {
                    printf(" %s=", attr->content.module->attributes[j]->name);
                    print_expr(attr->content.module->attributes[j]->expression);
                }
                printf(" ]");
                break;
            case LISBETH_SYNTHFILEBLOCKATTRIBUTE_CONNECTION:
                printf(" - %s:%s", attr->content.connection->module, attr->content.connection->port);
                break;
            case LISBETH_SYNTHFILEBLOCKATTRIBUTE_EXPRESSION:
                printf(" - ");
                print_expr(attr->content.expression);
                break;
            default:
                break;
        }
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    UNUSED(argc);
    UNUSED(argv);
    printf("Lisbeth Synth\n");

    char *text = NULL;
    size_t size = 0;
    SFile_loadRaw("test.lss", (uint8_t**)(&text), &size);
    if(!text)
        return EXIT_FAILURE;
    RREALLOC(text, char, size + 1);
    text[size] = '\0';
    size++;

    SLog_Logger *logger = SLog_logger_create(SLOG_LEVEL_ALL, false);
    SLog_logger_addOutput(logger, stdout);
    Lisbeth_SynthFileTranslator *translator;
    Lisbeth_SynthFileTokenList *tokens = Lisbeth_synthFile_parse(text, "test.lss", logger);
    if(tokens)
    {
        Lisbeth_SynthFileBlockList *blocks = Lisbeth_synthFileBuilder_build(tokens, "test.lss", logger);
        if(blocks)
        {
            for(size_t i = 0; i < blocks->blockCount; i++)
                print_block(blocks->blocks[i]);
            translator = Lisbeth_synthFileTranslator_create(blocks, logger);
            Lisbeth_SynthFileSource *source = Lisbeth_synthFileSource_create();
            char *programSelf = SFile_path_self();
            char *moduleLibrary = SFile_path_concat(".", "modules");
            Lisbeth_synthFileSource_add(source, moduleLibrary);
            FFREE(moduleLibrary);
            FFREE(programSelf);
            translator->source = source;
            Lisbeth_synthFileTranslator_translate(translator);
        }
        else
            printf("Pas blocks\n");
    }
    else
        printf("Pas tokens\n");


    return 0;
}
