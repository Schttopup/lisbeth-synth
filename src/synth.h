#ifndef LISBETH_SYNTH_SYNTH_H
#define LISBETH_SYNTH_SYNTH_H

#include "module.h"
#include "connection.h"
#include <svexec/memory.h>


struct Lisbeth_SynthModuleCode_
{
    char *fullName;
    SVExec_MemoryObject *memory;
    // TODO exports - add export support in SVExec (also SSF loading)
};

typedef struct Lisbeth_SynthModuleCode_ Lisbeth_SynthModuleCode;


struct Lisbeth_Synth_
{
    Lisbeth_SynthModule *rootModule;
    Lisbeth_SynthModuleCode **modulesCode;
    size_t modulesCodeCount;
};

typedef struct Lisbeth_Synth_ Lisbeth_Synth;


#endif //LISBETH_SYNTH_SYNTH_H
