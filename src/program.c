#include "program.h"


Lisbeth_SynthModuleProgram* Lisbeth_synthModuleProgram_create(SVExec_Processor *processor, SVExec_MemoryObject *memory)
{
    if(!processor || !memory)
        return NULL;
    Lisbeth_SynthModuleProgram *program = NULL;
    MMALLOC(program, Lisbeth_SynthModuleProgram, 1);
    program->processor = processor;
    program->memory = memory;
    return program;
}

void Lisbeth_synthModuleProgram_destroy(Lisbeth_SynthModuleProgram *program)
{
    if(!program)
        return;
    FFREE(program->processor);
    FFREE(program);
}