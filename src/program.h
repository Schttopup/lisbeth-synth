#ifndef LISBETH_SYNTH_PROGRAM_H
#define LISBETH_SYNTH_PROGRAM_H


#include <svexec/processor.h>


struct Lisbeth_SynthModuleProgram_
{
    SVExec_Processor *processor;
    SVExec_MemoryObject *memory;
};
typedef struct Lisbeth_SynthModuleProgram_ Lisbeth_SynthModuleProgram;


Lisbeth_SynthModuleProgram* Lisbeth_synthModuleProgram_create(SVExec_Processor *processor, SVExec_MemoryObject *memory);

void Lisbeth_synthModuleProgram_destroy(Lisbeth_SynthModuleProgram *group);


#endif //LISBETH_SYNTH_PROGRAM_H
