#include "module.h"
#include <sdefs.h>
#include <string.h>
#include "group.h"
#include "connection.h"
#include "program.h"


Lisbeth_SynthModule* Lisbeth_synthModule_create(char const *name, Lisbeth_SynthModuleType type, void *data)
{
    Lisbeth_SynthModule *module = NULL;
    MMALLOC(module, Lisbeth_SynthModule, 1);
    module->type = type;
    module->name = NULL;
    if(name)
    {
        MMALLOC(module->name, char, strlen(name) + 1);
        strcpy(module->name, name);
    }
    switch(type)
    {
        case LISBETH_SYNTHMODULE_NONE:
            break;
        case LISBETH_SYNTHMODULE_BUILTIN:
            module->content.builtin = (Lisbeth_SynthModuleBuiltin*)data;
            break;
        case LISBETH_SYNTHMODULE_BINARY:
            module->content.program = (Lisbeth_SynthModuleProgram*)data;
            break;
        case LISBETH_SYNTHMODULE_GROUP:
            module->content.group = (Lisbeth_SynthModuleGroup*)data;
            break;
        default:
            module->type = LISBETH_SYNTHMODULE_NONE;
            break;
    }
    module->inputCount = 0;
    module->inputs = NULL;
    module->outputCount = 0;
    module->outputs = NULL;
    return module;
}

void Lisbeth_synthModule_destroy(Lisbeth_SynthModule *module)
{
    if(!module)
        return;
    for(size_t i = 0; i < module->inputCount; i++)
    {
        FFREE(module->inputs[i]->name);
        FFREE(module->inputs[i]);
    }
    for(size_t i = 0; i < module->outputCount; i++)
    {
        FFREE(module->outputs[i]->name);
        FFREE(module->outputs[i]);
    }
    FFREE(module->name);
    switch(module->type)
    {
        case LISBETH_SYNTHMODULE_BINARY:
            Lisbeth_synthModuleProgram_destroy(module->content.program);
            break;
        case LISBETH_SYNTHMODULE_GROUP:
            Lisbeth_synthModuleGroup_destroy(module->content.group);
            break;
        case LISBETH_SYNTHMODULE_BUILTIN:
            (*module->content.builtin->destroy)(module);
            break;
        default:
            break;
    }
    FFREE(module);
}

struct Lisbeth_SynthModuleConnection_* Lisbeth_synthModule_getInputConnection(Lisbeth_SynthModule *module, char *name)
{
    if(!module || !name)
        return NULL;
    for(size_t i = 0; i < module->inputCount; i++)
    {
        if(strcmp(module->inputs[i]->name, name) == 0)
            return module->inputs[i];
    }
    return NULL;
}

struct Lisbeth_SynthModuleConnection_* Lisbeth_synthModule_getOutputConnection(Lisbeth_SynthModule *module, char *name)
{
    if(!module || !name)
        return NULL;
    for(size_t i = 0; i < module->outputCount; i++)
    {
        if(strcmp(module->outputs[i]->name, name) == 0)
            return module->outputs[i];
    }
    return NULL;
}

void Lisbeth_synthModule_addInputConnection(Lisbeth_SynthModule *module, struct Lisbeth_SynthModuleConnection_ *connection)
{
    if(!module || !connection)
        return;
    if(Lisbeth_synthModule_getInputConnection(module, connection->name))
        return;
    RREALLOC(module->inputs, struct Lisbeth_SynthModuleConnection_*, module->inputCount + 1);
    module->inputs[module->inputCount] = connection;
    module->inputCount++;
}

void Lisbeth_synthModule_addOutputConnection(Lisbeth_SynthModule *module, struct Lisbeth_SynthModuleConnection_ *connection)
{
    if(!module || !connection)
        return;
    if(Lisbeth_synthModule_getOutputConnection(module, connection->name))
        return;
    RREALLOC(module->outputs, struct Lisbeth_SynthModuleConnection_*, module->outputCount + 1);
    module->outputs[module->outputCount] = connection;
    module->outputCount++;
}

void Lisbeth_synthModule_tick(Lisbeth_SynthModule *module)
{
    // TODO
}


